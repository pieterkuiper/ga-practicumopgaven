package nl.hanze.web.ola;

import java.util.HashMap;
import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class AcceptGiroTransformer
{
    public AcceptGiroTransformer()
    {
        
    }
    
    public AcceptGiro transformAcceptGiro(HashMap<String, String> agHM)
    {
        AcceptGiro acceptGiro = new AcceptGiro();
        acceptGiro.setReference(Long.parseLong(agHM.get(Settings.REFERENCE)));
        acceptGiro.setEuro(Integer.parseInt(agHM.get(Settings.EURO)));
        acceptGiro.setCent(Integer.parseInt(agHM.get(Settings.CENT)));
        acceptGiro.setBetalingsKenmerk(transformBetalingsKenmerk(agHM.get(Settings.BETALINGSKENMERK)));
        acceptGiro.setRekeningNummer(agHM.get(Settings.REKENINGNUMMER));
        acceptGiro.setNaam(transformNaam(agHM.get(Settings.GESLACHT), agHM.get(Settings.INIT), agHM.get(Settings.ACHTERNAAM)));
        acceptGiro.setAdresPC(transformAdresPC(agHM.get(Settings.STRAATNAAM), agHM.get(Settings.STRAATNUMMER), agHM.get(Settings.POSTCODE)));
        acceptGiro.setPlaats(agHM.get(Settings.PLAATSNAAM));
        acceptGiro.setRekeningNummerNaar(agHM.get(Settings.REKENINGNUMMERNAAR));
        acceptGiro.setNaamNaar(agHM.get(Settings.NAAMNAAR));
        
        return acceptGiro;
    }
    
    private String transformBetalingsKenmerk(String betalingsKenmerk)
    {
        return betalingsKenmerk.replaceAll(".{4}", "$0 ");
    }
    
    private String transformNaam(String geslacht, String initialen, String naam)
    {
        StringBuilder sb = new StringBuilder();
        
        if (geslacht.equalsIgnoreCase("M"))
        {
            sb.append("Dhr. ");
        }
        else if (geslacht.equalsIgnoreCase("V"))
        {
            sb.append("Mevr. ");
        }
        
        sb.append(initialen);
        sb.append(" ");
        sb.append(naam);
        
        return sb.toString();
    }
    
    private String transformAdresPC(String straatnaam, String nummer, String postcode)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(straatnaam);
        sb.append(" ");
        sb.append(nummer);
        sb.append(" ");
        sb.append(transformPostcode(postcode));
        
        return sb.toString();
    }
    
    private String transformPostcode(String postcode)
    {
        return postcode.replaceAll(".{4}", "$0 ");
    }
}
