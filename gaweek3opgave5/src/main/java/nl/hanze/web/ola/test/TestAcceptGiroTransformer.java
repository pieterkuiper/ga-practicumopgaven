package nl.hanze.web.ola.test;

import java.util.HashMap;
import nl.hanze.web.ola.AcceptGiroTransformer;
import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class TestAcceptGiroTransformer
{
    public static void main(String[] args)
    {
        HashMap<String, String> agHM = new HashMap();
        agHM.put(Settings.REFERENCE, "123456");
        agHM.put(Settings.EURO, "12345");
        agHM.put(Settings.CENT, "0");
        agHM.put(Settings.BETALINGSKENMERK, "0000000000000000");
        agHM.put(Settings.REKENINGNUMMER, "");
        agHM.put(Settings.GESLACHT, "M");
        agHM.put(Settings.INIT, "P");
        agHM.put(Settings.ACHTERNAAM, "Kuiper");
        agHM.put(Settings.STRAATNAAM, "Korenbloemstraat");
        agHM.put(Settings.STRAATNUMMER, "24");
        agHM.put(Settings.POSTCODE, "9713PW");
        agHM.put(Settings.PLAATSNAAM, "Groningen");
        agHM.put(Settings.REKENINGNUMMERNAAR, "123456");
        agHM.put(Settings.NAAMNAAR, "Jansen");
        
        AcceptGiroTransformer agt = new AcceptGiroTransformer();
        AcceptGiro acceptGiro = agt.transformAcceptGiro(agHM);
        
        System.out.println(acceptGiro.getEuro());
        System.out.println(acceptGiro.getCent());
        System.out.println(acceptGiro.getBetalingsKenmerk());
        System.out.println(acceptGiro.getNaam());
        System.out.println(acceptGiro.getAdresPC());
    }
}
