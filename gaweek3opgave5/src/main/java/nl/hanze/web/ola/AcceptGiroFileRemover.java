package nl.hanze.web.ola;

import java.io.IOException;
import nl.hanze.web.ola.util.FileUtils;

/**
 *
 * @author Pieter
 */
public class AcceptGiroFileRemover
{
    public AcceptGiroFileRemover()
    {
        
    }
    
    public boolean removeFile(String file)
    {
        try
        {
            return FileUtils.deleteFile(file);
        }
        catch (IOException ex)
        {
            System.out.println("AcceptGiroFileRemover - removeFile - error: " + ex.getMessage());
        }
        
        return false;
    }
    
    public boolean moveFile(String file, String targetDir)
    {
        try
        {
            return FileUtils.moveFile(file, targetDir);
        }
        catch (IOException ex)
        {
            System.out.println("AcceptGiroFileRemover - moveFile - error: " + ex.getMessage());
        }
        
        return false;
    }
}
