package nl.hanze.web.ola.test;

import nl.hanze.web.ola.AcceptGiroPdfCreator;

public class TestAcceptGiroPdfCreator
{
    public static void main(String[] args) throws Exception
    {
        AcceptGiroPdfCreator agpc = new AcceptGiroPdfCreator("C:\\pdf", "C:\\pdf");
        agpc.createAcceptGiroPdf(1, 123, 5, "0000 0000 0000 0000", "P765672", 
                "Dhr. Rico Pipo", "Scheen 44 9999 HW", "Gramingen", 
                "987654", "Flipje Betuwe");
    }
}
