package nl.hanze.web.ola.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pieter
 */
public class FileUtils
{
    public static List<String> getFileNames(String fileDir, String extension) throws Exception
    {
        Path pathDir = FileSystems.getDefault().getPath(fileDir);
        
        if (!Files.exists(pathDir) || !Files.isDirectory(pathDir))
        {
            throw new Exception("Directory does not exist");
        }
        
        List<String> fileNames = new ArrayList<>();
        
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(pathDir, extension))
        {
            for (Path path : directoryStream)
            {
                fileNames.add(path.toString());
            }
        }
        return fileNames;
    }
    
    public static String readFileToString(String file) throws IOException
    {
        return new String(Files.readAllBytes(FileSystems.getDefault().getPath(file)));
    }
    
    public static List<String> readFileToList(String file) throws IOException
    {
        return Files.readAllLines(FileSystems.getDefault().getPath(file), StandardCharsets.UTF_8);
    }
    
    public static boolean moveFile(String file, String targetDir) throws IOException
    {
        Path pathFile = FileSystems.getDefault().getPath(file);
        Path pathTargetFile = FileSystems.getDefault().getPath(targetDir, pathFile.getFileName().toString());
        BasicFileAttributes afAttr = Files.readAttributes(pathFile, BasicFileAttributes.class);
        
        if (afAttr.isDirectory())
        {
            return false;
        }
        
        return Files.move(pathFile, pathTargetFile, StandardCopyOption.REPLACE_EXISTING).equals(pathTargetFile);
    }
    
    public static boolean deleteFile(String file) throws IOException
    {
        Path pathFile = FileSystems.getDefault().getPath(file);
        BasicFileAttributes afAttr = Files.readAttributes(pathFile, BasicFileAttributes.class);
        
        if (afAttr.isDirectory())
        {
            return false;
        }
        
        return Files.deleteIfExists(pathFile);
    }
}
