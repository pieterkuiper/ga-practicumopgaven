package nl.hanze.web.ola;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.FileUtils;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class AcceptGiroProcessor
{
    public AcceptGiroProcessor()
    {
        
    }
    
    public void process()
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SSS");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            long startTime = System.currentTimeMillis();

            AcceptGiroFileReader agfr = new AcceptGiroFileReader();
            AcceptGiroValidator agv = new AcceptGiroValidator();
            AcceptGiroFileRemover avr = new AcceptGiroFileRemover();
            AcceptGiroTransformer agt = new AcceptGiroTransformer();
            AcceptGiroPdfCreator agpc = new AcceptGiroPdfCreator(Settings.IMAGE_DIR, Settings.OUTBOX_DIR);
            
            List<String> fileNames = FileUtils.getFileNames(Settings.INBOX_DIR, "*.txt");
            for (String fileName : fileNames)
            {
                System.out.println("fileName: " + fileName);
                HashMap<String, String> agHM = agfr.readFile(fileName);
                boolean valid = agv.validateAcceptGiro(agHM);
                if (!valid)
                {
                    avr.moveFile(fileName, Settings.ERROR_DIR);
                    continue;
                }
                
                AcceptGiro acceptGiro = agt.transformAcceptGiro(agHM);
                
                agpc.createAcceptGiroPdf(acceptGiro.getReference(), acceptGiro.getEuro(), acceptGiro.getCent(), 
                        acceptGiro.getBetalingsKenmerk(), acceptGiro.getRekeningNummer(), acceptGiro.getNaam(), 
                        acceptGiro.getAdresPC(), acceptGiro.getPlaats(), acceptGiro.getRekeningNummerNaar(), 
                        acceptGiro.getNaamNaar());
                avr.removeFile(fileName);
            }
            
            System.out.println("Aantal bestanden verwerkt: " + fileNames.size());
            System.out.println("Vewerkingstijd: " + dateFormat.format(new Date(System.currentTimeMillis() - startTime)));
        }
        catch (Exception ex)
        {
            System.out.println("AcceptGiroProcessor - process - error: " + ex.getMessage());
        }
    }
}
