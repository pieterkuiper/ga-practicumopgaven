package nl.hanze.web.ola;

import java.util.Arrays;
import java.util.HashMap;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class AcceptGiroValidator
{
    public AcceptGiroValidator()
    {

    }
    
    public boolean validateAcceptGiro(HashMap<String, String> agHM)
    {
        if (!validateReference(agHM.get(Settings.REFERENCE)))
        {
            System.out.println(Settings.REFERENCE + " not valid");
            return false;
        }
        if (!validateEuro(agHM.get(Settings.EURO)))
        {
            System.out.println(Settings.EURO + " not valid");
            return false;
        }
        if (!validateCent(agHM.get(Settings.CENT)))
        {
            System.out.println(Settings.CENT + " not valid");
            return false;
        }
        if (!validateBetalingsKenmerk(agHM.get(Settings.BETALINGSKENMERK)))
        {
            System.out.println(Settings.BETALINGSKENMERK + " not valid");
            return false;
        }
        if (!validateRekeningNummer(agHM.get(Settings.REKENINGNUMMER)))
        {
            System.out.println(Settings.REKENINGNUMMER + " not valid");
            return false;
        }
        if (!validateGeslacht(agHM.get(Settings.GESLACHT)))
        {
            System.out.println(Settings.GESLACHT + " not valid");
            return false;
        }
        if (!validateInitialen(agHM.get(Settings.INIT)))
        {
            System.out.println(Settings.INIT + " not valid");
            return false;
        }
        if (!validateNaam(agHM.get(Settings.ACHTERNAAM)))
        {
            System.out.println(Settings.ACHTERNAAM + " not valid");
            return false;
        }
        if (!validateGeneral(agHM.get(Settings.STRAATNAAM)))
        {
            System.out.println(Settings.STRAATNAAM + " not valid");
            return false;
        }
        if (!validateNummer(agHM.get(Settings.STRAATNUMMER)))
        {
            System.out.println(Settings.STRAATNUMMER + " not valid");
            return false;
        }
        if (!validatePostcode(agHM.get(Settings.POSTCODE)))
        {
            System.out.println(Settings.POSTCODE + " not valid");
            return false;
        }
        if (!validateGeneral(agHM.get(Settings.PLAATSNAAM)))
        {
            System.out.println(Settings.PLAATSNAAM + " not valid");
            return false;
        }
        if (!validateRekeningNummerNaar(agHM.get(Settings.REKENINGNUMMERNAAR)))
        {
            System.out.println(Settings.REKENINGNUMMERNAAR + " not valid");
            return false;
        }
        if (!validateNaam(agHM.get(Settings.NAAMNAAR)))
        {
            System.out.println(Settings.NAAMNAAR + " not valid");
            return false;
        }
        
        System.out.println("Acceptgiro is valid");
        return true;
    }
    
    private boolean validateGeneral(String string)
    {
        if (!checkNotNull(string))
        {
            return false;
        }
        
        if (!checkLength(string, 1))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateReference(String reference)
    {
        if (!checkNotNull(reference))
        {
            return false;
        }
        
        if (!checkLong(reference))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateEuro(String euro)
    {
        if (!checkNotNull(euro))
        {
            return false;
        }
        
        if (!checkInt(euro))
        {
            return false;
        }
        
        if (!checkMinMax(Integer.parseInt(euro), 1, 99999))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateCent(String cent)
    {
        if (!checkNotNull(cent))
        {
            return false;
        }
        
        if (!checkInt(cent))
        {
            return false;
        }
        
        if (!checkMinMax(Integer.parseInt(cent), 0, 99))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateBetalingsKenmerk(String betalingsKenmerk)
    {
        if (!checkNotNull(betalingsKenmerk))
        {
            return false;
        }
        
        if (!checkLength(betalingsKenmerk, 16, 16))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateRekeningNummer(String rekeningNummer)
    {
        if (!checkNotNull(rekeningNummer))
        {
            return false;
        }
        
        if (!checkLength(rekeningNummer, 0, 10))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateGeslacht(String geslacht)
    {
        if (!checkNotNull(geslacht))
        {
            return false;
        }
        
        if (!checkInArray(geslacht, new String[] {"M","V"}))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateInitialen(String initialen)
    {
        if (!checkNotNull(initialen))
        {
            return false;
        }
        
        if (!checkLength(initialen, 1, 5))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateNaam(String naam)
    {
        if (!checkNotNull(naam))
        {
            return false;
        }
        
        if (!checkLength(naam, 1, 20))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateNummer(String nummer)
    {
        if (!checkNotNull(nummer))
        {
            return false;
        }
        
        if (!checkInt(nummer))
        {
            return false;
        }
        
        if (!checkMinMax(Integer.parseInt(nummer), 1, 9999))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validatePostcode(String postcode)
    {
        if (!checkNotNull(postcode))
        {
            return false;
        }
        
        if (!checkLength(postcode, 6, 6))
        {
            return false;
        }
        
        if (!checkInt(postcode.substring(0, 4)))
        {
            return false;
        }
        
        if (!checkMinMax(Integer.parseInt(postcode.substring(0, 4)), 1111, 9999))
        {
            return false;
        }
        
        if (!checkChar(postcode.substring(4, 6)))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean validateRekeningNummerNaar(String rekeningNummerNaar)
    {
        if (!checkNotNull(rekeningNummerNaar))
        {
            return false;
        }
        
        if (!checkLength(rekeningNummerNaar, 1, 10))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean checkNotNull(String string)
    {
        return string != null;
    }
    
    private boolean checkLong(String string)
    {
        try
        {
            Long.parseLong(string);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        
        return true;
    }
    
    private boolean checkInt(String string)
    {
        try
        {
            Integer.parseInt(string);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        
        return true;
    }
    
    private boolean checkMinMax(int integer, int min, int max)
    {
        return integer >= min && integer <= max;
    }
    
    private boolean checkLength(String string, int min)
    {
        return string.length() >= min;
    }
    
    private boolean checkLength(String string, int min, int max)
    {
        return string.length() >= min && string.length() <= max;
    }
    
    private boolean checkInArray(String string, String[] array)
    {
        return Arrays.asList(array).contains(string);
    }
    
    private boolean checkChar(String string)
    {
        for (int i = 0; i < string.length(); i++)
        {
            if (!Character.isLetter(string.charAt(i)))
            {
                return false;
            }
        }
        
        return true;
    }
}
