package nl.hanze.web.ola.util;

/**
 *
 * @author Pieter
 */
public class Settings
{
    //public static final String INBOX_DIR = System.getProperty("user.home") + System.getProperty("file.separator") + "OLA" + System.getProperty("file.separator") + "inbox";
    //public static final String OUTBOX_DIR = System.getProperty("user.home") + System.getProperty("file.separator") + "OLA" + System.getProperty("file.separator") + "outbox";
    //public static final String ERROR_DIR = System.getProperty("user.home") + System.getProperty("file.separator") + "OLA" + System.getProperty("file.separator") + "error";
    //public static final String IMAGE_DIR = System.getProperty("user.home") + System.getProperty("file.separator") + "OLA" + System.getProperty("file.separator") + "image";
    public static final String INBOX_DIR = "/Users/ola/OLA/inbox";
    public static final String OUTBOX_DIR = "/Users/ola/OLA/outbox";
    public static final String ERROR_DIR = "/Users/ola/OLA/error";
    public static final String IMAGE_DIR = "/Users/ola/OLA/image";
            
    public static final String GENERAL_DELIMITER = "=";
    public static final String BEDRAG_DELIMITER = ",";
    
    public static final long TIMER = 5 * 1000;
    
    public static final String REFERENCE = "REFERENCE";
    public static final String EURO = "EURO";
    public static final String CENT = "CENT";
    public static final String BETALINGSKENMERK = "BETALINGSKENMERK";
    public static final String REKENINGNUMMER = "REKENINGNUMMER";
    public static final String GESLACHT = "GESLACHT";
    public static final String INIT = "INIT";
    public static final String ACHTERNAAM = "ACHTERNAAM";
    public static final String STRAATNAAM = "STRAATNAAM";
    public static final String STRAATNUMMER = "STRAATNUMMER";
    public static final String POSTCODE = "POSTCODE";
    public static final String PLAATSNAAM = "PLAATSNAAM";
    public static final String REKENINGNUMMERNAAR = "REKENINGNUMMERNAAR";
    public static final String NAAMNAAR = "NAAMNAAR";
}
