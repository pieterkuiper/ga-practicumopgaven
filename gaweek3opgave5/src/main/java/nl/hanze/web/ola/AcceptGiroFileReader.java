package nl.hanze.web.ola;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import nl.hanze.web.ola.util.FileUtils;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class AcceptGiroFileReader
{    
    public AcceptGiroFileReader()
    {
        
    }
    
    public HashMap<String, String> readFile(String file)
    {
        HashMap<String, String> agHM = createEmptyAcceptGiroHashMap();
        try
        {
            List<String> lines = FileUtils.readFileToList(file);
            
            for (String line : lines)
            {
                String[] parts = line.split(Settings.GENERAL_DELIMITER);
                if (parts.length > 1)
                {
                    if (parts[0].equalsIgnoreCase("BEDRAG"))
                    {
                        String[] bedrag = parts[1].split(Settings.BEDRAG_DELIMITER);
                        if (bedrag.length > 0)
                        {
                            agHM.put("EURO", bedrag[0]);
                        }
                        if (bedrag.length > 1)
                        {
                            agHM.put("CENT", bedrag[1]);
                        }
                    }
                    else
                    {
                        agHM.put(parts[0].toUpperCase(), parts[1]);
                    }
                }
            }
        }
        catch (IOException ex)
        {
            System.out.println("AcceptGiroFileReader - readFile - error: " + ex.getMessage());
        }
        
        return agHM;
    }
    
    private HashMap<String, String> createEmptyAcceptGiroHashMap()
    {
        HashMap<String, String> agHM = new HashMap();
        agHM.put(Settings.REFERENCE, null);
        agHM.put(Settings.EURO, null);
        agHM.put(Settings.CENT, null);
        agHM.put(Settings.BETALINGSKENMERK, null);
        agHM.put(Settings.REKENINGNUMMER, "");
        agHM.put(Settings.GESLACHT, null);
        agHM.put(Settings.INIT, null);
        agHM.put(Settings.ACHTERNAAM, null);
        agHM.put(Settings.STRAATNAAM, null);
        agHM.put(Settings.STRAATNUMMER, null);
        agHM.put(Settings.POSTCODE, null);
        agHM.put(Settings.PLAATSNAAM, null);
        agHM.put(Settings.REKENINGNUMMERNAAR, null);
        agHM.put(Settings.NAAMNAAR, null);
        
        return agHM;
    }
}
