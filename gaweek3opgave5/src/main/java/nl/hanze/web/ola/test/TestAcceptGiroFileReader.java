package nl.hanze.web.ola.test;

import java.util.HashMap;
import java.util.Map;
import nl.hanze.web.ola.AcceptGiroFileReader;

/**
 *
 * @author Pieter
 */
public class TestAcceptGiroFileReader
{
    public static void main(String[] args) throws Exception
    {
        AcceptGiroFileReader agfr = new AcceptGiroFileReader();
        HashMap<String, String> agHM = agfr.readFile("C:\\Users\\Pieter\\OLA\\inbox\\test2.txt");
        for (Map.Entry entry : agHM.entrySet())
        {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
    }
}
