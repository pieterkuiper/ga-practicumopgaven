package nl.hanze.web.ola.main;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import nl.hanze.web.ola.AcceptGiroProcessor;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class AcceptGiroRunner
{
    public static void main(String[] args)
    {
        final AcceptGiroProcessor agp = new AcceptGiroProcessor();
        Timer timer = new Timer();
 
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run()
            {
                System.out.println(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Date()));
                agp.process();
            }
        }, 0, Settings.TIMER);
    }
}
