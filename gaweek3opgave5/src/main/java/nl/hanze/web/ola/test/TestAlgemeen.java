package nl.hanze.web.ola.test;

import java.util.List;
import nl.hanze.web.ola.util.FileUtils;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class TestAlgemeen
{
    public static void main(String[] args) throws Exception
    {
        System.out.println(Settings.INBOX_DIR);
        System.out.println(Settings.OUTBOX_DIR);
        List<String> fileNames = FileUtils.getFileNames(Settings.INBOX_DIR, "*.txt");
        for (String fileName : fileNames)
        {
            System.out.println(fileName);
        }
        
        List<String> lines = FileUtils.readFileToList("C:\\Users\\Pieter\\OLA\\inbox\\test1.txt");
            
        for (String line : lines)
        {
            System.out.println(line);
        }
        
        System.out.println(FileUtils.moveFile("C:\\Users\\Pieter\\OLA\\inbox\\test1.txt", Settings.ERROR_DIR));
    }
}
