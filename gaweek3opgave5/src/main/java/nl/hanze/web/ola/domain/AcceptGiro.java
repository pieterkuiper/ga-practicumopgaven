package nl.hanze.web.ola.domain;

/**
 *
 * @author Pieter
 */
public class AcceptGiro
{
    private long reference;
    private int euro;
    private int cent;
    private String betalingsKenmerk;
    private String rekeningNummer;
    private String naam;
    private String adresPC;
    private String plaats;
    private String rekeningNummerNaar;
    private String naamNaar;

    public long getReference()
    {
        return reference;
    }

    public void setReference(long reference)
    {
        this.reference = reference;
    }

    public int getEuro()
    {
        return euro;
    }

    public void setEuro(int euro)
    {
        this.euro = euro;
    }

    public int getCent()
    {
        return cent;
    }

    public void setCent(int cent)
    {
        this.cent = cent;
    }

    public String getBetalingsKenmerk()
    {
        return betalingsKenmerk;
    }

    public void setBetalingsKenmerk(String betalingsKenmerk)
    {
        this.betalingsKenmerk = betalingsKenmerk;
    }

    public String getRekeningNummer()
    {
        return rekeningNummer;
    }

    public void setRekeningNummer(String rekeningNummer)
    {
        this.rekeningNummer = rekeningNummer;
    }

    public String getNaam()
    {
        return naam;
    }

    public void setNaam(String naam)
    {
        this.naam = naam;
    }

    public String getAdresPC()
    {
        return adresPC;
    }

    public void setAdresPC(String adresPC)
    {
        this.adresPC = adresPC;
    }

    public String getPlaats()
    {
        return plaats;
    }

    public void setPlaats(String plaats)
    {
        this.plaats = plaats;
    }

    public String getRekeningNummerNaar()
    {
        return rekeningNummerNaar;
    }

    public void setRekeningNummerNaar(String rekeningNummerNaar)
    {
        this.rekeningNummerNaar = rekeningNummerNaar;
    }

    public String getNaamNaar()
    {
        return naamNaar;
    }

    public void setNaamNaar(String naamNaar)
    {
        this.naamNaar = naamNaar;
    }
}
