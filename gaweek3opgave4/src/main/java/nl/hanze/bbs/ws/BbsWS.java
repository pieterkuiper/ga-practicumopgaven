package nl.hanze.bbs.ws;

import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import nl.hanze.bbs.session.BbsTransSB;

/**
 *
 * @author Pieter
 */
@WebService(serviceName = "BbsWS")
public class BbsWS
{
    @EJB
    private BbsTransSB bbsTransSB;
    
    @WebMethod(operationName = "berekenBoete")
    public int berekenBoete(@WebParam(name = "bsn") int bsn, 
            @WebParam(name = "limiet") int limiet, @WebParam(name = "snelheid") int snelheid)
    {
        if (limiet > snelheid)
        {
            return 0;
        }
        return bbsTransSB.berekenBoete(bsn, limiet, snelheid);
    }
}
