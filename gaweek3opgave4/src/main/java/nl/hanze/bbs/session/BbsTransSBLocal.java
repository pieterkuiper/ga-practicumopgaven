package nl.hanze.bbs.session;

import javax.ejb.Local;
import nl.hanze.bbs.entity.Boete;

/**
 *
 * @author Pieter
 */
@Local
public interface BbsTransSBLocal
{
    public int berekenBoete(int bsn, int limiet, int snelheid);
    public void createBoete(Boete boete);
}
