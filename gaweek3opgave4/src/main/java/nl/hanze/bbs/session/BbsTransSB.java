package nl.hanze.bbs.session;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import nl.hanze.bbs.entity.Boete;
import nl.hanze.bbs.entity.Boetetabel;

/**
 *
 * @author Pieter
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@LocalBean
public class BbsTransSB implements BbsTransSBLocal
{
    private static final int SNELHEID_50 = 50;
    private static final int MAX_BOETEZWAARTEPUNT = 5;
    
    @PersistenceContext(unitName = "boeteRegistratiePU")
    private EntityManager em;

    @Override
    public int berekenBoete(int bsn, int limiet, int snelheid)
    {
        int boetebedrag = getBoetebedrag(limiet, snelheid);
        int boetebedragMet50kmOpslag = getBoetebedragMet50kmOpslag(limiet, boetebedrag);
        int boetezwaartepunt = berekenBoetezwaartepunt(bsn, new Date());
        int boetebedragTotaal = getBoetebedragTotaal(boetezwaartepunt, boetebedragMet50kmOpslag);
        
        System.out.println("boetebedrag: " + boetebedrag);
        System.out.println("boetebedragMet50kmOpslag: " + boetebedragMet50kmOpslag);
        System.out.println("boetezwaartepunt: " + boetezwaartepunt);
        System.out.println("boetebedragTotaal: " + boetebedragTotaal);
        
        return boetebedragTotaal;
    }
    
    @Override
    public void createBoete(Boete boete)
    {
        em.persist(boete);
    }

    private Boetetabel getBoetetabel(int snelheidsovertreding)
    {
        System.out.println("snelheidsovertreding: " + snelheidsovertreding);
        try
        {
            Query query = em.createQuery("SELECT b FROM Boetetabel b WHERE b.snelheidsovertreding <= :snelheidsovertreding ORDER BY b.snelheidsovertreding DESC");
            query.setParameter("snelheidsovertreding", snelheidsovertreding);
            query.setMaxResults(1);
            
            Boetetabel boetetabel = (Boetetabel) query.getSingleResult();
            
            return boetetabel;
        }
        catch (Exception ex)
        {
            System.out.println("error: " + ex.getMessage());
            return null;
        }
    }
    
    private int getBoetebedrag(int limiet, int snelheid)
    {
        return getBoetetabel(snelheid - limiet).getBoetebedrag();
    }
    
    private int getBoetebedragMet50kmOpslag(int limiet, int boetebedrag)
    {
        
        if (limiet <= SNELHEID_50)
        {
            boetebedrag *= 2;
        }
        return boetebedrag;
    }
    
    private int berekenBoetezwaartepunt(int bsn, Date datum)
    {
        int boetezwaartepunt = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datum);
        calendar.add(Calendar.YEAR, -1);
        System.out.println("Datum min 1 Jaar: " + calendar.getTime());
        Query query = em.createQuery("SELECT b FROM Boete b WHERE b.bsn = :bsn and b.datum >= :datum");
        query.setParameter("bsn", bsn);
        query.setParameter("datum", calendar.getTime());
        
        List<Boete> boetes = (List<Boete>) query.getResultList();
        for (Boete boete : boetes)
        {
            boetezwaartepunt += getBoetetabel(boete.getSnelheid() - boete.getLimiet()).getBoetezwaartepunt();
        }
        
        System.out.println("boetezwaartepunt voor aftrek: " + boetezwaartepunt);
        
        boetezwaartepunt -= MAX_BOETEZWAARTEPUNT;
        if (boetezwaartepunt < 0)
        {
            boetezwaartepunt = 0;
        }
        
        return boetezwaartepunt;
    }
    
    private int getBoetebedragTotaal(int boetezwaartepunt, int boetebedrag)
    {
        if (boetezwaartepunt > 0)
        {
            boetebedrag += (boetezwaartepunt * 0.5) * boetebedrag;
        }
        return boetebedrag;
    }
}
