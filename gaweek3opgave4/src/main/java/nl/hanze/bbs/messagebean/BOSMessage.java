package nl.hanze.bbs.messagebean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import nl.hanze.bbs.entity.Boete;
import nl.hanze.bbs.session.BbsTransSB;

/**
 *
 * @author Pieter
 */
@MessageDriven(mappedName = "jms/boeteafhandeling", activationConfig =
{
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/boeteafhandeling"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/boeteafhandeling")
})
public class BOSMessage implements MessageListener
{
    @EJB
    private BbsTransSB bbsTransSB;
    
    public BOSMessage()
    {
        
    }
    
    @Override
    public void onMessage(Message message)
    {
        if (message instanceof ObjectMessage)
        {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try
            {
                Boete boete = (Boete) objectMessage.getObject();
                bbsTransSB.createBoete(boete);
            }
            catch (JMSException ex)
            {
                System.out.println("error: " + ex.getMessage());
            }
        }
    }   
}
