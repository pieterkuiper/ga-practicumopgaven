package nl.hanze.bbs.messagebean;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import nl.hanze.bbs.entity.Boete;
import nl.hanze.bbs.session.BbsTransSB;

/**
 *
 * @author Pieter
 */
@MessageDriven(mappedName = "jms/boetebepaling", activationConfig =
{
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class BBSMessage implements MessageListener
{
    @EJB
    private BbsTransSB bbsTransSB;
    
    @Resource(mappedName = "jms/boeteafhandelingFactory")
    private ConnectionFactory connectionFactory;
    @Resource(mappedName="jms/boeteafhandeling")
    private Topic topic;
    
    public BBSMessage()
    {
        
    }
    
    @Override
    public void onMessage(Message message)
    {
        if (message instanceof TextMessage)
        {
            TextMessage textMessage = (TextMessage) message;
            try
            {
                int bsn = textMessage.getIntProperty("bsn");
                int limiet = textMessage.getIntProperty("limiet");
                int snelheid = textMessage.getIntProperty("snelheid");
                if (limiet < snelheid)
                {
                    int boetebedrag = bbsTransSB.berekenBoete(bsn, limiet, snelheid);
                    Boete boete = createBoete(bsn, snelheid, limiet, boetebedrag);
                    sendBoete(boete);
                }
            }
            catch (JMSException ex)
            {
                System.out.println("error: " + ex.getMessage());
            }
        }
    }
    
    private Boete createBoete(int bsn, int snelheid, int limiet, int boetebedrag)
    {
        Boete boete = new Boete();
        boete.setBsn(bsn);
        boete.setSnelheid(snelheid);
        boete.setLimiet(limiet);
        boete.setBoetebedrag(boetebedrag);
        boete.setDatum(new Date());
        
        return boete;
    }
    
    private void sendBoete(Boete boete) throws JMSException
    {
        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer messageProducer = session.createProducer(topic);
        ObjectMessage objectMessage = session.createObjectMessage();
        objectMessage.setObject(boete);
        messageProducer.send(objectMessage);
        messageProducer.close();
        connection.close();
    }
}
