package nl.hanze.bbs.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pieter
 */
@Entity
@Table(name = "boetetabel")
@XmlRootElement
public class Boetetabel implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "boetetabel_id")
    private Integer boetetabelId;
    @Column(name = "snelheidsovertreding")
    private Integer snelheidsovertreding;
    @Column(name = "boetebedrag")
    private Integer boetebedrag;
    @Column(name = "boetezwaartepunt")
    private Integer boetezwaartepunt;

    public Boetetabel()
    {
    }

    public Boetetabel(Integer boetetabelId)
    {
        this.boetetabelId = boetetabelId;
    }

    public Integer getBoetetabelId()
    {
        return boetetabelId;
    }

    public void setBoetetabelId(Integer boetetabelId)
    {
        this.boetetabelId = boetetabelId;
    }

    public Integer getSnelheidsovertreding()
    {
        return snelheidsovertreding;
    }

    public void setSnelheidsovertreding(Integer snelheidsovertreding)
    {
        this.snelheidsovertreding = snelheidsovertreding;
    }

    public Integer getBoetebedrag()
    {
        return boetebedrag;
    }

    public void setBoetebedrag(Integer boetebedrag)
    {
        this.boetebedrag = boetebedrag;
    }

    public Integer getBoetezwaartepunt()
    {
        return boetezwaartepunt;
    }

    public void setBoetezwaartepunt(Integer boetezwaartepunt)
    {
        this.boetezwaartepunt = boetezwaartepunt;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (boetetabelId != null ? boetetabelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boetetabel))
        {
            return false;
        }
        Boetetabel other = (Boetetabel) object;
        if ((this.boetetabelId == null && other.boetetabelId != null) || (this.boetetabelId != null && !this.boetetabelId.equals(other.boetetabelId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "nl.hanze.bbs.entity.Boetetabel[ boetetabelId=" + boetetabelId + " ]";
    }
    
}
