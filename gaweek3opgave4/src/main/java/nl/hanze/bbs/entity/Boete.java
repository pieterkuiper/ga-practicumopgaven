package nl.hanze.bbs.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pieter
 */
@Entity
@Table(name = "boete")
@XmlRootElement
public class Boete implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "boete_id")
    private Integer boeteId;
    @Column(name = "bsn")
    private Integer bsn;
    @Column(name = "snelheid")
    private Integer snelheid;
    @Column(name = "boetebedrag")
    private Integer boetebedrag;
    @Column(name = "limiet")
    private Integer limiet;
    @Column(name = "datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;

    public Boete()
    {
    }

    public Boete(Integer boeteId)
    {
        this.boeteId = boeteId;
    }

    public Integer getBoeteId()
    {
        return boeteId;
    }

    public void setBoeteId(Integer boeteId)
    {
        this.boeteId = boeteId;
    }

    public Integer getBsn()
    {
        return bsn;
    }

    public void setBsn(Integer bsn)
    {
        this.bsn = bsn;
    }

    public Integer getSnelheid()
    {
        return snelheid;
    }

    public void setSnelheid(Integer snelheid)
    {
        this.snelheid = snelheid;
    }

    public Integer getLimiet()
    {
        return limiet;
    }

    public void setLimiet(Integer limiet)
    {
        this.limiet = limiet;
    }

    public Integer getBoetebedrag()
    {
        return boetebedrag;
    }

    public void setBoetebedrag(Integer boetebedrag)
    {
        this.boetebedrag = boetebedrag;
    }

    public Date getDatum()
    {
        return datum;
    }

    public void setDatum(Date datum)
    {
        this.datum = datum;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (boeteId != null ? boeteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boete))
        {
            return false;
        }
        Boete other = (Boete) object;
        if ((this.boeteId == null && other.boeteId != null) || (this.boeteId != null && !this.boeteId.equals(other.boeteId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "nl.hanze.bbs.entity.Boete[ boeteId=" + boeteId + " ]";
    }
    
}
