#!/bin/bash
mydir="$(dirname "$BASH_SOURCE")"
echo $mydir/bin
cd $mydir/bin
echo Java RMI compiler - compileer HelloImpl
rmic -keep -v1.1 nl.hanze.web.rmi.HelloImpl
echo Java RMI compiler - HelloImpl gecompileerd
