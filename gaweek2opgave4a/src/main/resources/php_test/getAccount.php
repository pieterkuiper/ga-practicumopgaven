<?php
	$client = new SoapClient('http://localhost:8080/gaweek2opgave4a/CjibBankWS?wsdl', array('trace' => 1));
	
	print "FUNCTIONS OF THE SERVICE:\n";
	var_dump ($client->__getFunctions());
	
	print "DATATYPES OF THE SERVICE:\n";
	print_r($client->__getTypes());

	print "\nSOAP CALL:\n";
	$client->__soapCall('getAccount', array(
		'getAccount' => array(
			'reknummer' => '300000000')));
	
	print "\nSOAP REQUEST:\n";
	var_dump ($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump ($client->__getLastResponse());

	print "\n\n";
