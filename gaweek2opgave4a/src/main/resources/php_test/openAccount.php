<?php
	$client = new SoapClient('http://localhost:8080/gaweek2opgave4a/CjibBankWS?wsdl', array('trace' => 1));
	
	print "FUNCTIONS OF THE SERVICE:\n";
	var_dump ($client->__getFunctions());
	
	print "DATATYPES OF THE SERVICE:\n";
	print_r( $client->__getTypes());

	print "\nSOAP CALL:\n";
	$client->__soapCall('openAccount', array(
		'openAccount' => array(
			'account' => array(
				'rekeningnummer' => null, 
				'naam' => 'Pieter Kuiper', 
				'adres' => 'Korenbloemstraat 24', 
				'postcode' => '9713PW', 
				'woonplaats' => 'Groningen', 
				'saldo' => 10.00, 
				'limiet' => -200.00))));
	
	print "\nSOAP REQUEST:\n";
	var_dump ($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump ($client->__getLastResponse());

	print "\n\n";
