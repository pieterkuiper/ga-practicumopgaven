package nl.hanze.cjibbank.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import nl.hanze.cjibbank.entity.Account;

/**
 *
 * @author Pieter
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account>
{
    @PersistenceContext(unitName = "cjibbankPU1")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public AccountFacade()
    {
        super(Account.class);
    }
}
