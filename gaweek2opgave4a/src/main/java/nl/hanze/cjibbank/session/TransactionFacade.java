package nl.hanze.cjibbank.session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import nl.hanze.cjibbank.entity.Transaction;

/**
 *
 * @author Pieter
 */
@Stateless
public class TransactionFacade extends AbstractFacade<Transaction>
{
    @PersistenceContext(unitName = "cjibbankPU1")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public TransactionFacade()
    {
        super(Transaction.class);
    }
    
    public List<Transaction> getTransactions(String reknummer, String date)
    {
        System.out.println("date: " + date);
        
        List<Transaction> transactions = null;
        
        try
        {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date datum = df.parse(date);

            Query queryOntvangerVerzenderDatum = em.createQuery("SELECT t FROM Transaction t WHERE (t.ontvanger = :ontvanger OR t.verzender = :verzender) AND t.datum > :datum");
            queryOntvangerVerzenderDatum.setParameter("ontvanger", Integer.parseInt(reknummer));
            queryOntvangerVerzenderDatum.setParameter("verzender", Integer.parseInt(reknummer));
            queryOntvangerVerzenderDatum.setParameter("datum", datum);
            transactions = (List<Transaction>) queryOntvangerVerzenderDatum.getResultList();
        }
        catch (ParseException ex)
        {
            System.out.println(ex);
        }
        
        return transactions;
    }
}
