package nl.hanze.cjibbank.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import nl.hanze.cjibbank.entity.Account;

/**
 *
 * @author Pieter
 */
public class Test1
{
    public static void main(String[] args)
    {
        Account account = new Account();
        account.setNaam("Pieter Kuiper");
        account.setAdres("Korenbloemstraat 24");
        account.setPostcode("9713PW");
        account.setWoonplaats("Groningen");
        account.setSaldo(12f);
        account.setLimiet(-2000.00f);
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("cjibbankLOCAL");
        EntityManager em =  emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(account);
        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
