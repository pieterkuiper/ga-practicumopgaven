package nl.hanze.cjibbank.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pieter
 */
@Entity
@Table(name = "rekening")
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"),
    @NamedQuery(name = "Account.findByRekeningnummer", query = "SELECT a FROM Account a WHERE a.rekeningnummer = :rekeningnummer"),
    @NamedQuery(name = "Account.findByNaam", query = "SELECT a FROM Account a WHERE a.naam = :naam"),
    @NamedQuery(name = "Account.findByAdres", query = "SELECT a FROM Account a WHERE a.adres = :adres"),
    @NamedQuery(name = "Account.findByPostcode", query = "SELECT a FROM Account a WHERE a.postcode = :postcode"),
    @NamedQuery(name = "Account.findByWoonplaats", query = "SELECT a FROM Account a WHERE a.woonplaats = :woonplaats"),
    @NamedQuery(name = "Account.findBySaldo", query = "SELECT a FROM Account a WHERE a.saldo = :saldo"),
    @NamedQuery(name = "Account.findByLimiet", query = "SELECT a FROM Account a WHERE a.limiet = :limiet")
})
public class Account implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rekeningnummer")
    private Integer rekeningnummer;
    @Size(max = 45)
    @Column(name = "naam")
    private String naam;
    @Size(max = 45)
    @Column(name = "adres")
    private String adres;
    @Size(max = 45)
    @Column(name = "postcode")
    private String postcode;
    @Size(max = 45)
    @Column(name = "woonplaats")
    private String woonplaats;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "saldo")
    private Float saldo;
    @Column(name = "limiet")
    private Float limiet;

    public Account()
    {
    }

    public Account(Integer rekeningnummer)
    {
        this.rekeningnummer = rekeningnummer;
    }

    public Integer getRekeningnummer()
    {
        return rekeningnummer;
    }

    public void setRekeningnummer(Integer rekeningnummer)
    {
        this.rekeningnummer = rekeningnummer;
    }

    public String getNaam()
    {
        return naam;
    }

    public void setNaam(String naam)
    {
        this.naam = naam;
    }

    public String getAdres()
    {
        return adres;
    }

    public void setAdres(String adres)
    {
        this.adres = adres;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getWoonplaats()
    {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats)
    {
        this.woonplaats = woonplaats;
    }

    public Float getSaldo()
    {
        return saldo;
    }

    public void setSaldo(Float saldo)
    {
        this.saldo = saldo;
    }

    public Float getLimiet()
    {
        return limiet;
    }

    public void setLimiet(Float limiet)
    {
        this.limiet = limiet;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (rekeningnummer != null ? rekeningnummer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account))
        {
            return false;
        }
        Account other = (Account) object;
        if ((this.rekeningnummer == null && other.rekeningnummer != null) || (this.rekeningnummer != null && !this.rekeningnummer.equals(other.rekeningnummer)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "nl.hanze.cjibbank.entity.Account[ rekeningnummer=" + rekeningnummer + " ]";
    }
}
