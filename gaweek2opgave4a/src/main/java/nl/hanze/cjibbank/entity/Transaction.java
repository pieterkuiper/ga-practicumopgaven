package nl.hanze.cjibbank.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pieter
 */
@Entity
@Table(name = "transactie")
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t"),
    @NamedQuery(name = "Transaction.findByTransactionId", query = "SELECT t FROM Transaction t WHERE t.transactionId = :transactionId"),
    @NamedQuery(name = "Transaction.findByVerzender", query = "SELECT t FROM Transaction t WHERE t.verzender = :verzender"),
    @NamedQuery(name = "Transaction.findByOntvanger", query = "SELECT t FROM Transaction t WHERE t.ontvanger = :ontvanger"),
    @NamedQuery(name = "Transaction.findByBedrag", query = "SELECT t FROM Transaction t WHERE t.bedrag = :bedrag"),
    @NamedQuery(name = "Transaction.findByDatum", query = "SELECT t FROM Transaction t WHERE t.datum = :datum")
})
public class Transaction implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "transaction_id")
    private Integer transactionId;
    @Column(name = "verzender")
    private Integer verzender;
    @Column(name = "ontvanger")
    private Integer ontvanger;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "bedrag")
    private Float bedrag;
    @Column(name = "datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;

    public Transaction()
    {
    }

    public Transaction(Integer transactionId)
    {
        this.transactionId = transactionId;
    }

    public Integer getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId)
    {
        this.transactionId = transactionId;
    }

    public Integer getVerzender()
    {
        return verzender;
    }

    public void setVerzender(Integer verzender)
    {
        this.verzender = verzender;
    }

    public Integer getOntvanger()
    {
        return ontvanger;
    }

    public void setOntvanger(Integer ontvanger)
    {
        this.ontvanger = ontvanger;
    }

    public Float getBedrag()
    {
        return bedrag;
    }

    public void setBedrag(Float bedrag)
    {
        this.bedrag = bedrag;
    }

    public Date getDatum()
    {
        return datum;
    }

    public void setDatum(Date datum)
    {
        this.datum = datum;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaction))
        {
            return false;
        }
        Transaction other = (Transaction) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "nl.hanze.cjibbank.entity.Transaction[ transactionId=" + transactionId + " ]";
    }
}
