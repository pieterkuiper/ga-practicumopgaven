package nl.hanze.cjibbank.ws;

import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import nl.hanze.cjibbank.entity.Account;
import nl.hanze.cjibbank.entity.Transaction;
import nl.hanze.cjibbank.session.AccountFacade;
import nl.hanze.cjibbank.session.TransactionFacade;

/**
 *
 * @author Pieter
 */
@WebService(serviceName = "CjibBankWS")
public class CjibBankWS
{
    @EJB
    private AccountFacade accountFacade;
    @EJB
    private TransactionFacade transactionFacade;
    
    /**
     * Web service operation
     * @param account
     */
    @WebMethod(operationName = "openAccount")
    public void openAccount(@WebParam(name = "account") Account account)
    {
        if (account != null)
        {
            accountFacade.create(account);
        }
    }
    
    /**
     * Web service operation
     * @param amount
     * @param reknummerFrom
     * @param reknummerTo
     * @return 
     */
    @WebMethod(operationName = "transfer")
    public boolean transfer(@WebParam(name = "amount") float amount,
            @WebParam(name = "reknummerFrom") String reknummerFrom,
            @WebParam(name = "reknummerTo") String reknummerTo)
    {
        if (amount != 0.0f && reknummerFrom != null && reknummerTo != null)
        {
            try
            {
                int rekeningnummerFrom = Integer.parseInt(reknummerFrom);
                int rekeningnummerTo = Integer.parseInt(reknummerTo);
                
                Account accountFrom = accountFacade.find(rekeningnummerFrom);
                
                // Controleer of het rekeningnummer bestaat. Als accountFrom null is, bestaat het rekeningnummer niet
                if (accountFrom == null)
                {
                    System.out.println("accountFrom");
                    return false;
                }
                
                float nieuwSaldoFrom = accountFrom.getSaldo() - amount;
                System.out.println("amount: " + amount);
                System.out.println("saldoFrom: " + accountFrom.getSaldo());
                System.out.println("nieuwSaldoFrom: " + nieuwSaldoFrom);
                System.out.println("LimietFrom: " + accountFrom.getLimiet());
                
                // Controleer of het saldo toereikend is
                if (nieuwSaldoFrom < accountFrom.getLimiet())
                {
                    System.out.println("saldoFrom");
                    return false;
                }
                
                Account accountTo = accountFacade.find(rekeningnummerTo);
                
                // Controleer of het rekeningnummer bestaat. Als accountTo null is, bestaat het rekeningnummer niet
                if (accountTo == null)
                {
                    System.out.println("accountTo");
                    return false;
                }
                
                float nieuwSaldoTo = accountTo.getSaldo() + amount;
                
                accountFrom.setSaldo(nieuwSaldoFrom);
                accountTo.setSaldo(nieuwSaldoTo);
                
                Transaction transaction = new Transaction();
                transaction.setVerzender(rekeningnummerFrom);
                transaction.setOntvanger(rekeningnummerTo);
                transaction.setBedrag(amount);
                transaction.setDatum(new Date());
                
                accountFacade.edit(accountFrom);
                accountFacade.edit(accountTo);
                transactionFacade.create(transaction);
                
                return true;
            }
            catch (NumberFormatException ex)
            {
                System.out.println(ex);
            }
        }
        
        return false;
    }
    
    /**
     * Web service operation
     * @param reknummer
     * @return 
     */
    @WebMethod(operationName = "getAccount")
    public Account getAccount(@WebParam(name = "reknummer") String reknummer)
    {
        if (reknummer != null)
        {
            try
            {
                int rekeningnummer = Integer.parseInt(reknummer);
                return accountFacade.find(rekeningnummer);
            }
            catch (NumberFormatException ex)
            {
                System.out.println(ex);
            }
        }
        
        return null;
    }
    
    /**
     * Web service operation
     * @param account
     */
    @WebMethod(operationName = "alterAccount")
    public void alterAccount(@WebParam(name = "account") Account account)
    {
        if (account != null)
        {
            accountFacade.edit(account);
        }
    }
    
    /**
     * Web service operation
     * @param reknummer
     * @param date
     * @return 
     */
    @WebMethod(operationName = "getTransactions")
    public List<Transaction> getTransactions(@WebParam(name = "reknummer") String reknummer,
            @WebParam(name = "date") String date)
    {
        if (reknummer != null && date != null)
        {
            return transactionFacade.getTransactions(reknummer, date);
        }
        
        return null;
    }
}
