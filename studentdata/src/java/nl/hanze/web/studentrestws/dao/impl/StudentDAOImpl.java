package nl.hanze.web.studentrestws.dao.impl;

import java.sql.*;
import java.util.*;
import nl.hanze.web.studentrestws.dao.*;
import nl.hanze.web.studentrestws.domain.*;

public class StudentDAOImpl implements StudentDAO {
    private Connection conn;
    private Statement s;

    @Override
    public List<Student> getAllStudents() throws Exception {
        openConnection();
        ResultSet rs=s.executeQuery("select * from student");
        ArrayList<Student> students=new ArrayList<Student>();
        while (rs.next()) {
            int SID=rs.getInt(1);
            int stdNr=rs.getInt(2);
            String name=rs.getString(3);
            Student student=new Student();
            student.setSID(SID);
            student.setStdnr(stdNr);
            student.setName(name);
            students.add(student);
        }
        closeConnection();
        return students;
    }

    @Override
    public Student getStudentByStdNr(int stdNr) throws Exception {
        openConnection();
        ResultSet rs=s.executeQuery("select * from student where `StdNr`="+stdNr);
        Student student=null;
        if (rs.next()) {
            int SID=rs.getInt(1);
            String name=rs.getString(3);
            student=new Student();
            student.setSID(SID);
            student.setStdnr(stdNr);
            student.setName(name);
        }
        closeConnection();
        return student;
     }

    @Override
    public void saveOrUpdate(Student student) throws Exception {
        if (student.getSID()==-1) {
            openConnection();
            s.executeUpdate("insert into student(`StdNr`, `Name`) Values ("+student.getStdnr()+",'"+student.getName()+"')");
            ResultSet rs=s.executeQuery("select last_insert_id()");
            rs.next();
            int SID=rs.getInt(1);
            student.setSID(SID);
            closeConnection();
        } else {
            openConnection();
            s.executeUpdate("update student set StdNr="+student.getStdnr()+", Name='"+student.getName()+"' where SID="+student.getSID());
            closeConnection();
        }
    }

    @Override
    public void deleteByStdNr(int stdNr) throws Exception {
        openConnection();
        s.executeUpdate("delete from student where StdNr="+stdNr);
        closeConnection();
    }

    private void openConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        conn=DriverManager.getConnection("jdbc:mysql://localhost/student", "student_hanze", "student_hanze");
        s=conn.createStatement();
    }

    private void closeConnection() throws Exception {
        s.close();
        conn.close();
    }
}

