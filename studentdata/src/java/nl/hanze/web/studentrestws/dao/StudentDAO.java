package nl.hanze.web.studentrestws.dao;

import java.util.*;
import nl.hanze.web.studentrestws.domain.*;

public interface StudentDAO {
    public List<Student> getAllStudents() throws Exception;
    public Student getStudentByStdNr(int stdNr) throws Exception;
    public void saveOrUpdate(Student student) throws Exception;
    public void deleteByStdNr(int stdNr) throws Exception;
}
