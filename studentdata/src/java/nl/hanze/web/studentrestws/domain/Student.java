package nl.hanze.web.studentrestws.domain;

public class Student {
    private int SID;
    private int stdnr;
    private String name;

    public Student() {
        SID=-1;
    }

    public void setSID(int SID) {
        this.SID=SID;
    }

    public int getSID() {
        return SID;
    }

    public int getStdnr() {
        return stdnr;
    }

    public void setStdnr(int stdnr) {
        this.stdnr = stdnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

