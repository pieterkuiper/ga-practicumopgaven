package nl.hanze.web.studentrestws.resource;

import javax.ws.rs.*;
import nl.hanze.web.studentrestws.binding.*;
import nl.hanze.web.studentrestws.dao.*;
import nl.hanze.web.studentrestws.dao.impl.*;
import nl.hanze.web.studentrestws.domain.*;

@Path("/student/")
public class StudentResource {
    @GET
    @Path("{stdnr}")
    @Produces("application/xml")
    public StudentBinding get(@PathParam("stdnr") int stdnr) throws Exception {
        StudentDAO sdao=new StudentDAOImpl();
        Student student=sdao.getStudentByStdNr(stdnr);
        return new StudentBinding(student);
    }

    @PUT
    @Consumes("application/xml")
    public String put(StudentBinding sb) throws Exception {
        Student student=sb.getStudent();
        StudentDAO sdao=new StudentDAOImpl();
        sdao.saveOrUpdate(student);
        return "<status>OK</status>";
    }

    @DELETE
    @Path("{stdnr}")
    public String delete(@PathParam("stdnr") int stdnr) throws Exception  {
        StudentDAO sdao=new StudentDAOImpl();
        sdao.deleteByStdNr(stdnr);
        return "<status>OK</status>";
    }
}


