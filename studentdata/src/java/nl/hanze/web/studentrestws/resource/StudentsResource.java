package nl.hanze.web.studentrestws.resource;

import javax.ws.rs.*;
import java.util.*;
import nl.hanze.web.studentrestws.domain.*;
import nl.hanze.web.studentrestws.binding.*;
import nl.hanze.web.studentrestws.dao.*;
import nl.hanze.web.studentrestws.dao.impl.*;

@Path("/students/")
public class StudentsResource {
    @GET
    @Produces("application/xml")
    public StudentsBinding getAll() throws Exception {
        StudentDAO sdao=new StudentDAOImpl();
        List<Student> students=sdao.getAllStudents();
        return new StudentsBinding(students);
    }
}
