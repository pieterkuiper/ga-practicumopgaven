package nl.hanze.web.studentrestws.binding;

import java.util.*;
import javax.xml.bind.annotation.*;
import nl.hanze.web.studentrestws.domain.*;

@XmlRootElement(name="students")
public class StudentsBinding {
    private List<Student> students;

    public StudentsBinding() {}

    public StudentsBinding(List<Student> students) {
        this.students=students;
    }

    @XmlElement
    public List<StudentBinding> getStudent() {
        ArrayList<StudentBinding> sbindings=new ArrayList<StudentBinding>();
        for(Student student : students)
            sbindings.add(new StudentBinding(student));

        return sbindings;
    }
}

