/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.hanze.web.studentrestws.binding;

import javax.xml.bind.annotation.*;
import nl.hanze.web.studentrestws.domain.*;


@XmlRootElement(name="student")
public class StudentBinding {
    private Student student;

    public StudentBinding() {}

    public StudentBinding(Student student) {
        this.student=student;
    }

    @XmlElement
    public int getStdnr() {
        if (student!=null) return student.getStdnr(); else return -1;
    }

    @XmlElement
    public String getName() {
        if (student!=null) return student.getName(); else return "Unknown";
    }

    public void setStdnr(int stdnr) {
        if (student==null) student=new Student();
        student.setStdnr(stdnr);
    }

    public void setName(String name) {
        if (student==null) student=new Student();
        student.setName(name);
    }

    public Student getStudent() {
        return student;
    }
}

