<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if(empty($_POST['accountnumber'])){
		echo 'Rekeningnummer mag niet leeg zijn!';
		die();
	}	
	
	$options = array('trace' => 1);
	$client = new SoapClient('http://145.37.87.24:8080/gaweek2opgave2/JavaBankWS?wsdl', $options);
	
	$response = $client->alterAccount(array(
		'account' => array(
			'rekeningnummer' => $_POST['accountnumber'],
			'naam' => $_POST['name'],
			'adres' => $_POST['address'],
			'postcode' => $_POST['zipcode'],
			'woonplaats' => $_POST['city'],
			'saldo' => $_POST['balance'],
			'limiet' => $_POST['limit']
		)
	));
	
	echo 'Verzoek is gestuurd.';
	die();
}
?>
<html>
	<head>
		<title>Alter Account - Hanze</title>
	</head>
	<body>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<fieldset>
				Rekeningnummer: <input type="text" name="accountnumber" /><br />
			</fieldset>
			<br />
			<fieldset>
				Name: <input type="text" name="name" /><br />
				Adres: <input type="text" name="address" /><br />
				Postcode: <input type="text" name="zipcode" /><br />
				Woonplaats: <input type="text" name="city" /><br />
				Saldo: <input type="text" name="balance" /><br />
				Limiet: <input type="text" name="limit" /><br />
				<input type="submit" value="Aanmaken" name="btnOpenAccount" />
			</fieldset>
		</form>
	</body>
</html>