<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	$options = array('trace' => 1);
	$client = new SoapClient('http://145.37.87.24:8080/gaweek2opgave2/JavaBankWS?wsdl', $options);
	
	$response = $client->transfer(array(
		'amount' => $_POST['amount'],
		'reknummerFrom' => $_POST['from'],
		'reknummerTo' => $_POST['to']
	));
	
	echo 'Verzoek is verstuurd.<br />';
	
	$data = $response->return;
	if($data){
		echo 'Betaling is uitgevoerd.';
	} else {
		echo 'Er is iets fout gegaan tijdens de betaling. Controleer de gegevens.';
	}
	die();
	
endif;
?>
<html>
	<head>
		<title>Transfer Currency</title>
	</head>
	<body>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<fieldset>
				Van: <input type="text" name="from" /><br />
				Naar: <input type="text" name="to" /><br />
				Bedrag: <input type="text" name="amount" /><br />
				<input type="submit" value="Verzenden" name="btnTransfer" />
			</fieldset>
		</form>
	</body>
</html>