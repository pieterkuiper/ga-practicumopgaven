<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'):
	$options = array('trace' => 1);
	$client = new SoapClient('http://145.37.87.24:8080/gaweek2opgave2/JavaBankWS?wsdl', $options);
	
	$response = $client->openAccount(array(
		'account' => array(
			'rekeningnummer' => null,
			'naam' => $_POST['name'],
			'adres' => $_POST['address'],
			'postcode' => $_POST['zipcode'],
			'woonplaats' => $_POST['city'],
			'saldo' => $_POST['balance'],
			'limiet' => $_POST['limit']
		)
	));
	
	echo 'Verzoek is gestuurd.';
	die();
		
endif;
?>
<html>
	<head>
		<title>Open Account</title>
	</head>
	<body>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<fieldset>
				Name: <input type="text" name="name" /><br />
				Adres: <input type="text" name="address" /><br />
				Postcode: <input type="text" name="zipcode" /><br />
				Woonplaats: <input type="text" name="city" /><br />
				Saldo: <input type="text" name="balance" /><br />
				Limiet: <input type="text" name="limit" /><br />
				<input type="submit" value="Aanmaken" name="btnOpenAccount" />
			</fieldset>
		</form>
	</body>
</html>