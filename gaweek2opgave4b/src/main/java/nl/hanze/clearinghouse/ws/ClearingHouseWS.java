package nl.hanze.clearinghouse.ws;

import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import nl.hanze.clearinghouse.session.DisTransSB;

/**
 *
 * @author Pieter
 */
@WebService(serviceName = "ClearingHouseWS")
public class ClearingHouseWS
{
    @EJB
    private DisTransSB disTransSB;
    
    /**
     * Web service operation
     * @param amount
     * @param reknummerFrom
     * @param reknummerTo
     * @return 
     */
    @WebMethod(operationName = "transferJavaBankToCjibBank")
    public boolean transferJavaBankToCjibBank(@WebParam(name = "amount") float amount,
            @WebParam(name = "reknummerFrom") String reknummerFrom,
            @WebParam(name = "reknummerTo") String reknummerTo)
    {
        if (amount != 0.0f && reknummerFrom != null && reknummerTo != null)
        {
            return disTransSB.transferJavaBankToCjibBank(amount, reknummerFrom, reknummerTo);
        }
        
        return false;
    }
    
    /**
     * Web service operation
     * @param amount
     * @param reknummerFrom
     * @param reknummerTo
     * @return 
     */
    @WebMethod(operationName = "transferCjibBankToJavaBank")
    public boolean transferCjibBankToJavaBank(@WebParam(name = "amount") float amount,
            @WebParam(name = "reknummerFrom") String reknummerFrom,
            @WebParam(name = "reknummerTo") String reknummerTo)
    {
        if (amount != 0.0f && reknummerFrom != null && reknummerTo != null)
        {
            return disTransSB.transferCjibBankToJavaBank(amount, reknummerFrom, reknummerTo);
        }
        
        return false;
    }
}
