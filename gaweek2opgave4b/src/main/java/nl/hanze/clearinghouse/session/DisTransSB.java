package nl.hanze.clearinghouse.session;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import nl.hanze.clearinghouse.entity.AccountCjibBank;
import nl.hanze.clearinghouse.entity.AccountJavaBank;
import nl.hanze.clearinghouse.entity.TransactionCjibBank;
import nl.hanze.clearinghouse.entity.TransactionJavaBank;

/**
 *
 * @author Pieter
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@LocalBean
public class DisTransSB implements DisTransSBLocal
{
    @Resource
    private EJBContext ejbContext;
    
    @PersistenceContext(unitName = "javabankPU")
    private EntityManager emJavaBank;

    @PersistenceContext(unitName = "cjibbankPU")
    private EntityManager emCjibBank;

    @Override
    public boolean transferJavaBankToCjibBank(float amount, String reknummerFrom, String reknummerTo)
    {
        try
        {
            AccountJavaBank accountJavaBank = getAccountJavaBank(reknummerFrom);
            // Controleer of het rekeningnummer bestaat. Als accountJavaBank null is, bestaat het rekeningnummer niet
            if (accountJavaBank == null)
            {
                throw new Exception("reknummerFrom: " + reknummerFrom + " bestaat niet");
            }

            float nieuwSaldoFrom = accountJavaBank.getSaldo() - amount;
            System.out.println("amount: " + amount);
            System.out.println("saldoFrom: " + accountJavaBank.getSaldo());
            System.out.println("nieuwSaldoFrom: " + nieuwSaldoFrom);
            System.out.println("LimietFrom: " + accountJavaBank.getLimiet());

            // Controleer of het saldo toereikend is
            if (nieuwSaldoFrom < accountJavaBank.getLimiet())
            {
                throw new Exception("saldoFrom is niet toereikend");
            }

            AccountCjibBank accountCjibBank = getAccountCjibBank(reknummerTo);

            // Controleer of het rekeningnummer bestaat. Als accountCjibBank null is, bestaat het rekeningnummer niet
            if (accountCjibBank == null)
            {
                throw new Exception("reknummerTo: " + reknummerTo + " bestaat niet");
            }

            float nieuwSaldoTo = accountCjibBank.getSaldo() + amount;

            accountJavaBank.setSaldo(nieuwSaldoFrom);
            updateAccountJavaBank(accountJavaBank);
            
            accountCjibBank.setSaldo(nieuwSaldoTo);
            updateAccountCjibBank(accountCjibBank);
            
            TransactionJavaBank transactionJavaBank = new TransactionJavaBank();
            transactionJavaBank.setVerzender(accountJavaBank.getRekeningnummer());
            transactionJavaBank.setOntvanger(accountCjibBank.getRekeningnummer());
            transactionJavaBank.setBedrag(amount);
            transactionJavaBank.setDatum(new Date());
            createTransactionJavaBank(transactionJavaBank);
            
            TransactionCjibBank transactionCjibBank = new TransactionCjibBank();
            transactionCjibBank.setVerzender(accountJavaBank.getRekeningnummer());
            transactionCjibBank.setOntvanger(accountCjibBank.getRekeningnummer());
            transactionCjibBank.setBedrag(amount);
            transactionCjibBank.setDatum(new Date());
            createTransactionCjibBank(transactionCjibBank);
        }
        catch (Exception ex)
        {
            ejbContext.setRollbackOnly();
            System.out.println("error: " + ex.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public boolean transferCjibBankToJavaBank(float amount, String reknummerFrom, String reknummerTo)
    {
        try
        {
            AccountCjibBank accountCjibBank = getAccountCjibBank(reknummerFrom);
            
            
            // Controleer of het rekeningnummer bestaat. Als accountCjibBank null is, bestaat het rekeningnummer niet
            if (accountCjibBank == null)
            {
                throw new Exception("reknummerFrom: " + reknummerFrom + " bestaat niet");
            }

            float nieuwSaldoFrom = accountCjibBank.getSaldo() - amount;
            System.out.println("amount: " + amount);
            System.out.println("saldoFrom: " + accountCjibBank.getSaldo());
            System.out.println("nieuwSaldoFrom: " + nieuwSaldoFrom);
            System.out.println("LimietFrom: " + accountCjibBank.getLimiet());

            // Controleer of het saldo toereikend is
            if (nieuwSaldoFrom < accountCjibBank.getLimiet())
            {
                throw new Exception("saldoFrom is niet toereikend");
            }

            AccountJavaBank accountJavaBank = getAccountJavaBank(reknummerTo);

            // Controleer of het rekeningnummer bestaat. Als accountJavaBank null is, bestaat het rekeningnummer niet
            if (accountJavaBank == null)
            {
                throw new Exception("reknummerTo: " + reknummerTo + " bestaat niet");
            }

            float nieuwSaldoTo = accountJavaBank.getSaldo() + amount;

            accountCjibBank.setSaldo(nieuwSaldoFrom);
            updateAccountCjibBank(accountCjibBank);
            
            accountJavaBank.setSaldo(nieuwSaldoTo);
            updateAccountJavaBank(accountJavaBank);
            
            TransactionCjibBank transactionCjibBank = new TransactionCjibBank();
            transactionCjibBank.setVerzender(accountCjibBank.getRekeningnummer());
            transactionCjibBank.setOntvanger(accountJavaBank.getRekeningnummer());
            transactionCjibBank.setBedrag(amount);
            transactionCjibBank.setDatum(new Date());
            createTransactionCjibBank(transactionCjibBank);
            
            TransactionJavaBank transactionJavaBank = new TransactionJavaBank();
            transactionJavaBank.setVerzender(accountCjibBank.getRekeningnummer());
            transactionJavaBank.setOntvanger(accountJavaBank.getRekeningnummer());
            transactionJavaBank.setBedrag(amount);
            transactionJavaBank.setDatum(new Date());
            createTransactionJavaBank(transactionJavaBank);
        }
        catch (Exception ex)
        {
            ejbContext.setRollbackOnly();
            System.out.println("error: " + ex.getMessage());
            return false;
        }

        return true;
    }
    
    private AccountJavaBank getAccountJavaBank(String reknummer)
    {
        return emJavaBank.find(AccountJavaBank.class, Integer.parseInt(reknummer));
    }
    
    private AccountCjibBank getAccountCjibBank(String reknummer)
    {
        return emCjibBank.find(AccountCjibBank.class, Integer.parseInt(reknummer));
    }
    
    private void updateAccountJavaBank(AccountJavaBank accountJavaBank)
    {
        emJavaBank.merge(accountJavaBank);
    }
    
    private void updateAccountCjibBank(AccountCjibBank accountCjibBank)
    {
        emCjibBank.merge(accountCjibBank);
    }
    
    private void createTransactionJavaBank(TransactionJavaBank transactionJavaBank)
    {
        emJavaBank.persist(transactionJavaBank);
    }
    
    private void createTransactionCjibBank(TransactionCjibBank transactionCjibBank)
    {
        emCjibBank.persist(transactionCjibBank);
    }
}
