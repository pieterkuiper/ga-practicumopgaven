package nl.hanze.clearinghouse.session;

import javax.ejb.Local;

/**
 *
 * @author Pieter
 */
@Local
public interface DisTransSBLocal
{
    public boolean transferJavaBankToCjibBank(float amount, String reknummerFrom, String reknummerTo);
    public boolean transferCjibBankToJavaBank(float amount, String reknummerFrom, String reknummerTo);
}
