package nl.hanze.clearinghouse.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pieter
 */
@Entity
@Table(name = "rekening")
@XmlRootElement
public class AccountJavaBank implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rekeningnummer")
    private Integer rekeningnummer;
    @Size(max = 45)
    @Column(name = "naam")
    private String naam;
    @Size(max = 45)
    @Column(name = "adres")
    private String adres;
    @Size(max = 45)
    @Column(name = "postcode")
    private String postcode;
    @Size(max = 45)
    @Column(name = "woonplaats")
    private String woonplaats;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "saldo")
    private Float saldo;
    @Column(name = "limiet")
    private Float limiet;

    public AccountJavaBank()
    {
    }

    public AccountJavaBank(Integer rekeningnummer)
    {
        this.rekeningnummer = rekeningnummer;
    }

    public Integer getRekeningnummer()
    {
        return rekeningnummer;
    }

    public void setRekeningnummer(Integer rekeningnummer)
    {
        this.rekeningnummer = rekeningnummer;
    }

    public String getNaam()
    {
        return naam;
    }

    public void setNaam(String naam)
    {
        this.naam = naam;
    }

    public String getAdres()
    {
        return adres;
    }

    public void setAdres(String adres)
    {
        this.adres = adres;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getWoonplaats()
    {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats)
    {
        this.woonplaats = woonplaats;
    }

    public Float getSaldo()
    {
        return saldo;
    }

    public void setSaldo(Float saldo)
    {
        this.saldo = saldo;
    }

    public Float getLimiet()
    {
        return limiet;
    }

    public void setLimiet(Float limiet)
    {
        this.limiet = limiet;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (rekeningnummer != null ? rekeningnummer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountJavaBank))
        {
            return false;
        }
        AccountJavaBank other = (AccountJavaBank) object;
        if ((this.rekeningnummer == null && other.rekeningnummer != null) || (this.rekeningnummer != null && !this.rekeningnummer.equals(other.rekeningnummer)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "nl.hanze.clearinghouse.entity.AccountJavaBank[ rekeningnummer=" + rekeningnummer + " ]";
    }
}
