package nl.hanze.web.gba.domain;

/**
 *
 * @author Pieter
 */
public class NatuurlijkPersoon
{
    private long bsn;
    private char geslacht;
    private String initialen;
    private String achternaam;
    private String straatnaam;
    private int nummer;
    private String postcode;
    private String woonplaats;

    /**
     * 
     * @return 
     */
    public long getBsn()
    {
        return bsn;
    }

    /**
     * 
     * @param bsn 
     */
    public void setBsn(long bsn)
    {
        this.bsn = bsn;
    }

    /**
     * 
     * @return 
     */
    public char getGeslacht()
    {
        return geslacht;
    }

    /**
     * 
     * @param geslacht 
     */
    public void setGeslacht(char geslacht)
    {
        this.geslacht = geslacht;
    }

    /**
     * 
     * @return 
     */
    public String getInitialen()
    {
        return initialen;
    }

    /**
     * 
     * @param initialen 
     */
    public void setInitialen(String initialen)
    {
        this.initialen = initialen;
    }

    /**
     * 
     * @return 
     */
    public String getAchternaam()
    {
        return achternaam;
    }

    public void setAchternaam(String achternaam)
    {
        this.achternaam = achternaam;
    }

    /**
     * 
     * @return 
     */
    public String getStraatnaam()
    {
        return straatnaam;
    }

    /**
     * 
     * @param straatnaam 
     */
    public void setStraatnaam(String straatnaam)
    {
        this.straatnaam = straatnaam;
    }

    public int getNummer()
    {
        return nummer;
    }

    /**
     * 
     * @param nummer 
     */
    public void setNummer(int nummer)
    {
        this.nummer = nummer;
    }

    public String getPostcode()
    {
        return postcode;
    }

    /**
     * 
     * @param postcode 
     */
    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    /**
     * 
     * @return 
     */
    public String getWoonplaats()
    {
        return woonplaats;
    }

    /**
     * 
     * @param woonplaats 
     */
    public void setWoonplaats(String woonplaats)
    {
        this.woonplaats = woonplaats;
    }
}