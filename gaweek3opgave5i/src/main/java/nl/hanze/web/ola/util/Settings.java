package nl.hanze.web.ola.util;

/**
 *
 * @author Pieter
 */
public class Settings
{
    public final static String HOST = "localhost";
    public final static String USERNAME = "ola";
    public final static String PASSWORD = "ola";
    public final static String INBOX = "OLA/inbox/";
    
    public static final String GENERAL_DELIMITER = "=";
    public static final String BEDRAG_DELIMITER = ",";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    public static final String REFERENCE = "REFERENCE";
    public static final String BEDRAG = "BEDRAG";
    public static final String BETALINGSKENMERK = "BETALINGSKENMERK";
    public static final String REKENINGNUMMER = "REKENINGNUMMER";
    public static final String GESLACHT = "GESLACHT";
    public static final String INIT = "INIT";
    public static final String ACHTERNAAM = "ACHTERNAAM";
    public static final String STRAATNAAM = "STRAATNAAM";
    public static final String STRAATNUMMER = "STRAATNUMMER";
    public static final String POSTCODE = "POSTCODE";
    public static final String PLAATSNAAM = "PLAATSNAAM";
    public static final String REKENINGNUMMERNAAR = "REKENINGNUMMERNAAR";
    public static final String NAAMNAAR = "NAAMNAAR";
    
    public final static String URL = "http://localhost:8080/gba/GBA";
    public static final String CJIB = "CJIB";
    public static final String CJIB_REKENINGNUMMER = "569988888";
}
