package nl.hanze.web.ola.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author Pieter
 */
public class FtpUploader
{
    FTPClient ftpClient = new FTPClient();
    
    public FtpUploader(String host, String username, String password) throws IOException, Exception
    {
        ftpClient = new FTPClient();
        ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        ftpClient.connect(host);
        int reply = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply))
        {
            ftpClient.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
        ftpClient.login(username, password);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
    }
    
    public void uploadFile(String hostDir, String fileName, String fileContent) throws Exception
    {
        InputStream inputStream = new ByteArrayInputStream(fileContent.getBytes());
        ftpClient.storeFile(hostDir + fileName, inputStream);
    }
    
    public void disconnect() throws IOException
    {
        if (ftpClient.isConnected())
        {
            ftpClient.logout();
            ftpClient.disconnect();
        }
    }
}
