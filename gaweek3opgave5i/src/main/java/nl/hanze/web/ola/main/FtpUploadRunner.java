package nl.hanze.web.ola.main;

import nl.hanze.web.ola.client.FileContentCreator;
import nl.hanze.web.ola.client.FtpUploader;
import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author Pieter
 */
public class FtpUploadRunner
{
    public static void main(String[] args) throws Exception
    {
        AcceptGiro acceptGiro = new AcceptGiro();
        acceptGiro.setReference(1L);
        acceptGiro.setEuro(10);
        acceptGiro.setCent(75);
        acceptGiro.setBetalingsKenmerk("0000111122223333");
        acceptGiro.setRekeningNummer("30000000");
        acceptGiro.setGeslacht("M");
        acceptGiro.setInitialen("P");
        acceptGiro.setAchternaam("Kuiper");
        acceptGiro.setStraatnaam("Korenbloemstraat");
        acceptGiro.setStraatnummer(24);
        acceptGiro.setPostcode("9713PW");
        acceptGiro.setPlaatsnaam("Groningen");
        acceptGiro.setRekeningNummerNaar("12345678");
        acceptGiro.setNaamNaar("Jan Jansen");
        
        FileContentCreator fcc = new FileContentCreator();
        String fileContent = fcc.createFileContent(acceptGiro);
        
        FtpUploader ftpUploader = new FtpUploader(Settings.HOST, Settings.USERNAME, Settings.PASSWORD);
        ftpUploader.uploadFile(Settings.INBOX, acceptGiro.getReference() + ".txt", fileContent);
        ftpUploader.disconnect();
        System.out.println("Done");
    }
}
