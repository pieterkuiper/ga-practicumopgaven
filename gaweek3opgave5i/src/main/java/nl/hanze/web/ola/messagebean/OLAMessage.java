package nl.hanze.web.ola.messagebean;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import nl.hanze.bbs.entity.Boete;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;
import nl.hanze.web.ola.client.FileContentCreator;
import nl.hanze.web.ola.client.FtpUploader;
import nl.hanze.web.ola.client.JsonClient;
import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.Settings;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Pieter
 */
@MessageDriven(mappedName = "jms/boeteafhandeling", activationConfig =
{
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/boeteafhandeling"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/boeteafhandeling")
})
public class OLAMessage implements MessageListener
{
    private int reference = 1;
    
    public OLAMessage()
    {
        
    }
    
    @Override
    public void onMessage(Message message)
    {
        if (message instanceof ObjectMessage)
        {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try
            {
                Boete boete = (Boete) objectMessage.getObject();
                
                JsonClient jsonClient = new JsonClient();
                NatuurlijkPersoon natuurlijkPersoon = jsonClient.getNatuurlijkPersoon(boete.getBsn());
                
                AcceptGiro acceptGiro = createAcceptGiro(natuurlijkPersoon, boete);
                
                FileContentCreator fcc = new FileContentCreator();
                String fileContent = fcc.createFileContent(acceptGiro);
                
                FtpUploader ftpUploader = new FtpUploader(Settings.HOST, Settings.USERNAME, Settings.PASSWORD);
                ftpUploader.uploadFile(Settings.INBOX, acceptGiro.getReference() + ".txt", fileContent);
                ftpUploader.disconnect();
                
            }
            catch (JMSException | IOException ex)
            {
                System.out.println("error: " + ex.getMessage());
            }
            catch (Exception ex)
            {
                Logger.getLogger(OLAMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private AcceptGiro createAcceptGiro(NatuurlijkPersoon natuurlijkPersoon, Boete boete)
    {
        AcceptGiro acceptGiro = new AcceptGiro();
        acceptGiro.setReference(reference);
        acceptGiro.setEuro(boete.getBoetebedrag());
        acceptGiro.setCent(0);
        acceptGiro.setBetalingsKenmerk(createBetalingsKenmerk(reference));
        acceptGiro.setRekeningNummer("");
        acceptGiro.setGeslacht(String.valueOf(natuurlijkPersoon.getGeslacht()));
        acceptGiro.setInitialen(natuurlijkPersoon.getInitialen());
        acceptGiro.setAchternaam(natuurlijkPersoon.getAchternaam());
        acceptGiro.setStraatnaam(natuurlijkPersoon.getStraatnaam());
        acceptGiro.setStraatnummer(natuurlijkPersoon.getNummer());
        acceptGiro.setPostcode(natuurlijkPersoon.getPostcode());
        acceptGiro.setPlaatsnaam(natuurlijkPersoon.getWoonplaats());
        acceptGiro.setRekeningNummerNaar(Settings.CJIB_REKENINGNUMMER);
        acceptGiro.setNaamNaar(Settings.CJIB);
        reference ++;
        
        return acceptGiro;
    }
    
    private String createBetalingsKenmerk(int boeteId)
    {
        return StringUtils.leftPad(Integer.toString(boeteId), 16, "0");
    }
}
