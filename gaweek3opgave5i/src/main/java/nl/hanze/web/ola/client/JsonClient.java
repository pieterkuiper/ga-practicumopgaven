package nl.hanze.web.ola.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;
import nl.hanze.web.ola.util.Settings;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Pieter
 */
public class JsonClient
{
    public NatuurlijkPersoon getNatuurlijkPersoon(int bsn) throws IOException
    {
        NatuurlijkPersoon natuurlijkPersoon = null;
        
        Client client = Client.create();
        WebResource webResource = client.resource(Settings.URL);
        ClientResponse clientResponse = webResource
                .queryParam("action", "get")
                .queryParam("bsn", Integer.toString(bsn))
                .accept("application/json").get(ClientResponse.class);
        
        if (clientResponse.getStatus() != 200)
        {
            System.out.println("Failed : HTTP error code : " + clientResponse.getStatus());
            return natuurlijkPersoon;
        }
        
        String output = clientResponse.getEntity(String.class);
        System.out.println(output);
        String json = output.replace("natuurlijkpersoon", "NatuurlijkPersoon");
        
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
        natuurlijkPersoon = objectMapper.readValue(json, NatuurlijkPersoon.class);
        
        return natuurlijkPersoon;
    }
    
    public static void main(String[] args) throws IOException
    {
        JsonClient jsonClient = new JsonClient();
        NatuurlijkPersoon natuurlijkPersoon = jsonClient.getNatuurlijkPersoon(123456789);
        
        if (natuurlijkPersoon != null)
        {
            System.out.println("bsn: " + natuurlijkPersoon.getBsn());
            System.out.println("geslacht: " + natuurlijkPersoon.getGeslacht());
            System.out.println("initialen: " + natuurlijkPersoon.getInitialen());
            System.out.println("achternaam: " + natuurlijkPersoon.getAchternaam());
            System.out.println("straatnaam: " + natuurlijkPersoon.getStraatnaam());
            System.out.println("nummer: " + natuurlijkPersoon.getNummer());
            System.out.println("postcode: " + natuurlijkPersoon.getPostcode());
            System.out.println("woonplaats: " + natuurlijkPersoon.getWoonplaats());
        }
        else
        {
            System.out.println("null");
        }
    }
}
