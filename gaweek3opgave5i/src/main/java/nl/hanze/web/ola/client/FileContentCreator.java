package nl.hanze.web.ola.client;

import nl.hanze.web.ola.domain.AcceptGiro;
import nl.hanze.web.ola.util.Settings;

/**
 *
 * @author pieter
 */
public class FileContentCreator
{
    public String createFileContent(AcceptGiro acceptGiro)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(Settings.REFERENCE);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getReference());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.BEDRAG);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getEuro());
        sb.append(Settings.BEDRAG_DELIMITER);
        sb.append(acceptGiro.getCent());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.BETALINGSKENMERK);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getBetalingsKenmerk());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.REKENINGNUMMER);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getRekeningNummer());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.GESLACHT);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getGeslacht());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.INIT);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getInitialen());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.ACHTERNAAM);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getAchternaam());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.STRAATNAAM);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getStraatnaam());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.STRAATNUMMER);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getStraatnummer());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.POSTCODE);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getPostcode());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.PLAATSNAAM);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getPlaatsnaam());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.REKENINGNUMMERNAAR);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getRekeningNummerNaar());
        sb.append(Settings.LINE_SEPARATOR);
        
        sb.append(Settings.NAAMNAAR);
        sb.append(Settings.GENERAL_DELIMITER);
        sb.append(acceptGiro.getNaamNaar());
        
        return sb.toString();
    }
}
