package nl.hanze.web.ola.domain;

/**
 *
 * @author Pieter
 */
public class AcceptGiro
{
    private long reference;
    private int euro;
    private int cent;
    private String betalingsKenmerk;
    private String rekeningNummer;
    private String geslacht;
    private String achternaam;
    private String initialen;
    private String straatnaam;
    private int straatnummer;
    private String postcode;
    private String plaatsnaam;
    private String rekeningNummerNaar;
    private String naamNaar;

    public long getReference()
    {
        return reference;
    }

    public void setReference(long reference)
    {
        this.reference = reference;
    }

    public int getEuro()
    {
        return euro;
    }

    public void setEuro(int euro)
    {
        this.euro = euro;
    }

    public int getCent()
    {
        return cent;
    }

    public void setCent(int cent)
    {
        this.cent = cent;
    }

    public String getBetalingsKenmerk()
    {
        return betalingsKenmerk;
    }

    public void setBetalingsKenmerk(String betalingsKenmerk)
    {
        this.betalingsKenmerk = betalingsKenmerk;
    }

    public String getRekeningNummer()
    {
        return rekeningNummer;
    }

    public void setRekeningNummer(String rekeningNummer)
    {
        this.rekeningNummer = rekeningNummer;
    }

    public String getGeslacht()
    {
        return geslacht;
    }

    public void setGeslacht(String geslacht)
    {
        this.geslacht = geslacht;
    }

    public String getAchternaam()
    {
        return achternaam;
    }

    public void setAchternaam(String achternaam)
    {
        this.achternaam = achternaam;
    }
    
    public String getInitialen()
    {
        return initialen;
    }

    public void setInitialen(String initialen)
    {
        this.initialen = initialen;
    }

    public String getStraatnaam()
    {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam)
    {
        this.straatnaam = straatnaam;
    }

    public int getStraatnummer()
    {
        return straatnummer;
    }

    public void setStraatnummer(int straatnummer)
    {
        this.straatnummer = straatnummer;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getPlaatsnaam()
    {
        return plaatsnaam;
    }

    public void setPlaatsnaam(String plaatsnaam)
    {
        this.plaatsnaam = plaatsnaam;
    }

    public String getRekeningNummerNaar()
    {
        return rekeningNummerNaar;
    }

    public void setRekeningNummerNaar(String rekeningNummerNaar)
    {
        this.rekeningNummerNaar = rekeningNummerNaar;
    }

    public String getNaamNaar()
    {
        return naamNaar;
    }

    public void setNaamNaar(String naamNaar)
    {
        this.naamNaar = naamNaar;
    }
}
