package nl.hanze.bbs.entity;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Pieter
 */
public class Boete implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Integer boeteId;
    private Integer bsn;
    private Integer snelheid;
    private Integer boetebedrag;
    private Integer limiet;
    private Date datum;

    public Boete()
    {
    }

    public Boete(Integer boeteId)
    {
        this.boeteId = boeteId;
    }

    public Integer getBoeteId()
    {
        return boeteId;
    }

    public void setBoeteId(Integer boeteId)
    {
        this.boeteId = boeteId;
    }

    public Integer getBsn()
    {
        return bsn;
    }

    public void setBsn(Integer bsn)
    {
        this.bsn = bsn;
    }

    public Integer getSnelheid()
    {
        return snelheid;
    }

    public void setSnelheid(Integer snelheid)
    {
        this.snelheid = snelheid;
    }

    public Integer getLimiet()
    {
        return limiet;
    }

    public void setLimiet(Integer limiet)
    {
        this.limiet = limiet;
    }

    public Integer getBoetebedrag()
    {
        return boetebedrag;
    }

    public void setBoetebedrag(Integer boetebedrag)
    {
        this.boetebedrag = boetebedrag;
    }

    public Date getDatum()
    {
        return datum;
    }

    public void setDatum(Date datum)
    {
        this.datum = datum;
    }
}
