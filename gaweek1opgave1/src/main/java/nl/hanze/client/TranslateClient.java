package nl.hanze.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import nl.hanze.util.Settings;

/**
 *
 * @author Pieter
 */
public class TranslateClient
{
    Socket clientSocket = null;
    PrintWriter out = null;
    BufferedReader in = null;
    
    public static void main(String[] args)
    {
        new TranslateClient();
    }
    
    public TranslateClient()
    {
        makeConnection();

        try
        {
            handleRequests();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
	
    private void makeConnection()
    {
        try
        {
            clientSocket = new Socket(Settings.HOSTNAME, Settings.PORT_NUM);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }
        catch (UnknownHostException ex)
        {
            System.err.println("Don't know about host: " + Settings.HOSTNAME);
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.err.println("Couldn't get I/O for the connection to: " + Settings.HOSTNAME);
            System.exit(1);
        }
    }
	
    private void handleRequests() throws IOException
    {
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String fromServer;
        String fromUser;

        while ((fromServer = in.readLine()) != null)
        {
            System.out.println("Server: " + fromServer);
            if (fromServer.equals(Settings.STATE_QUIT))
            {
                break;
            }

            fromUser = stdIn.readLine();
            if (fromUser != null)
            {
                System.out.println("Client: " + fromUser);
                out.println(fromUser);
            }
        }
        
        out.close();
        in.close();
        stdIn.close();
        clientSocket.close();
    }
}
