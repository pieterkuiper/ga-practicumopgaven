package nl.hanze.protocol;

import nl.hanze.translate.Translate;
import nl.hanze.translate.TranslateEnglish;
import nl.hanze.translate.TranslateGerman;
import nl.hanze.util.Settings;

/**
 *
 * @author Pieter
 */
public class TranslateProtocol
{
    private static final int WAITING = 0;
    private static final int LANGUAGE = 1;
    private static final int TRANSLATE = 2;
    private static final int ANOTHER = 3;

    private Translate translate = null;
    
    private int state = WAITING;
    
    public String processInput(String theInput)
    {
        String theOutput = null;
        
        if (state == WAITING)
        {
            theOutput = "Naar welke taal wil je een woord of zin vertalen?";
            state = LANGUAGE;
        }
        else if (state == LANGUAGE)
        {
            switch (theInput.toLowerCase())
            {
                case "engels":
                    translate = new TranslateEnglish();
                    theOutput = "Welk woord of welke zin wil je naar het engels vertalen?";
                    state = TRANSLATE;
                    break;
                case "duits":
                    translate = new TranslateGerman();
                    theOutput = "Welk of welke zin wil je naar het duits vertalen?";
                    state = TRANSLATE;
                    break;
                case "stop":
                    theOutput = Settings.STATE_QUIT;
                    state = WAITING;
                    break;
                default:
                    translate = null;
                    theOutput = "De taal " + theInput + " wordt niet ondersteund. Je kunt kiezen uit de volgende talen: engels en duits.";
                    state = LANGUAGE;
            }
        }
        else if (state == TRANSLATE)
        {
            String[] words = theInput.split(" ");
            
            StringBuilder translation = new StringBuilder();
            for (int i = 0; i < words.length; i++)
            {
                translation.append(translate.translateWord(words[i]));
                //translation.append(translate.translateWordMicrosoft(words[i]));
                if (words.length > i + 1)
                {
                    translation.append(" ");
                }
            }
            
            if (words.length > 1)
            {
                translation.append(".");
            }
            
            translation.append(" - Wil je nog een woord of zin naar het ");
            translation.append(translate.getLanguage());
            translation.append(" vertalen? (j/n)");
            
            theOutput = translation.toString();
            state = ANOTHER;
        }
        else if (state == ANOTHER)
        {
            if (theInput.equalsIgnoreCase("j"))
            {
                theOutput = "Welk woord wil je naar het " + translate.getLanguage() + " vertalen?";
                state = TRANSLATE;
            }
            else
            {
                theOutput = "Naar welke taal wil je een woord of zin vertalen? Om te stoppen kunt je \"stop\" typen.";
                state = LANGUAGE;
            }
        }
        
        return theOutput;
    }
}
