package nl.hanze.server;

import nl.hanze.protocol.TranslateProtocol;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import nl.hanze.util.Settings;

/**
 *
 * @author Pieter
 */
public class TranslateServer
{
    private ServerSocket serverSocket = null;
    
    public static void main(String[] args)
    {
        TranslateServer translateServer = new TranslateServer();
        translateServer.acceptIncomingConnections();
    }
    
    public TranslateServer()
    {

    }
    
    public void acceptIncomingConnections()
    {
        try
        {
            makeListener();
            while(true)
            {
                Socket clientSocket = serverSocket.accept();
                handleIncomingConnection(clientSocket);
            }
        }
        catch (IOException ex)
        {
            System.err.println("Accept failed.");
            System.exit(1);
        }
    }
    
    private void makeListener()
    {
        try
        {
            serverSocket = new ServerSocket(Settings.PORT_NUM);
            System.out.println("Server waiting connections on port " + Settings.PORT_NUM);
        }
        catch (IOException ex)
        {
            System.err.println("Could not listen on port: " + Settings.PORT_NUM);
            System.exit(1);
        }
    }
    
    protected void handleIncomingConnection(Socket clientSocket)
    {
        new Thread(new ConnectionHandler(clientSocket)).start();
    }
    
    private static class ConnectionHandler implements Runnable
    {
        private Socket clientSocket;
        private TranslateProtocol tp = null;

        public ConnectionHandler(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
            tp = new TranslateProtocol();
        }
        
        private void handleRequests() throws IOException
        {
            String inputLine;
            String outputLine;
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            outputLine = tp.processInput(null);
            out.println(outputLine);

            while ((inputLine = in.readLine()) != null)
            {
                outputLine = tp.processInput(inputLine);
                out.println(outputLine);
                if (outputLine.equals(Settings.STATE_QUIT))
                {
                    break;
                }
            }
            out.close();
            in.close();
            clientSocket.close();
        }

        @Override
        public void run()
        {
            try
            {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
                System.out.println("New client connected: " + simpleDateFormat.format(new Date()));
                System.out.println("Handling client from " + clientSocket.getInetAddress().getHostAddress() + " on port " + clientSocket.getLocalPort() + " - " + clientSocket.getPort() + " by thread " + Thread.currentThread().getName());
                handleRequests();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
