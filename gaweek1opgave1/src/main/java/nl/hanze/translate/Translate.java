package nl.hanze.translate;

import java.util.HashMap;

/**
 *
 * @author Pieter
 */
public class Translate 
{
    protected HashMap<String, String> translation;
    protected String language;
    
    public Translate()
    {
        translation = new HashMap<>();
    }

    public String getLanguage()
    {
        return language;
    }

    public String translateWord(String word)
    {
        if (translation.containsKey(word.toLowerCase()))
        {
            return translation.get(word.toLowerCase());
        }
        else
        {
            return "?" + word + "?";
        }
    }
    
    public String translateWordMicrosoft(String word)
    {
        if (translation.containsKey(word.toLowerCase()))
        {
            return translation.get(word.toLowerCase());
        }
        else
        {
            MicrosoftTranslator microsoftTranslator = new MicrosoftTranslator();
            return microsoftTranslator.translate(language, word);
        }
    }
}
