package nl.hanze.translate;

/**
 *
 * @author Pieter
 */
public class TranslateEnglish extends Translate
{   
    public TranslateEnglish()
    {
        language = "engels";
        
        translation.put("de", "the");
        translation.put("het", "it");
        translation.put("een", "a");
        translation.put("ik", "I");
        translation.put("heb", "have");
        translation.put("land", "country");
        translation.put("dag", "day");
        translation.put("leeftijd", "age");
        translation.put("kleding", "clothing");
        translation.put("blauw", "blue");
        translation.put("woord", "word");
        translation.put("dit", "this");
        translation.put("wil", "want");
        translation.put("hallo", "hello");
        translation.put("hoe", "how");
        translation.put("gaat", "goes");
    }
}
