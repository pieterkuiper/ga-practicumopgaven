package nl.hanze.translate;

/**
 *
 * @author Pieter
 */
public class TranslateGerman extends Translate
{
    public TranslateGerman()
    {
        language = "duits";
        
        translation.put("de", "die");
        translation.put("het", "die");
        translation.put("een", "ein");
        translation.put("ik", "ich");
        translation.put("heb", "habe");
        translation.put("land", "Land");
        translation.put("dag", "Tag");
        translation.put("leeftijd", "Alter");
        translation.put("kleding", "Kleidung");
        translation.put("blauw", "blau");
        translation.put("woord", "Wort");
        translation.put("dit", "dies");
        translation.put("wil", "werden");
        translation.put("hallo", "Hallo");
        translation.put("hoe", "wie");
        translation.put("gaat", "ist");
    }
}
