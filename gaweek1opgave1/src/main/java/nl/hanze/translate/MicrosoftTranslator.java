package nl.hanze.translate;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.hanze.util.Settings;

/**
 *
 * @author Pieter
 */
public class MicrosoftTranslator
{
    public static void main(String[] args) throws Exception
    {
        MicrosoftTranslator microsoftTranslator = new MicrosoftTranslator();

        System.out.println(microsoftTranslator.translate("duits", "fiets"));
    }
    
    public String translate(String language, String word)
    {
        Translate.setClientId(Settings.MS_CLIENTID);
        Translate.setClientSecret(Settings.MS_CLIENTSECRET);
        Language toLanguage;
        switch (language)
        {
            case "engels":
                toLanguage = Language.ENGLISH;
                break;
            case "duits":
                toLanguage = Language.GERMAN;
                break;
            default:
                toLanguage = Language.ENGLISH;
        }
        
        try
        {
            return Translate.execute(word, Language.DUTCH, toLanguage).toLowerCase();
        }
        catch (Exception ex)
        {
            return "???";
        }
    }
}