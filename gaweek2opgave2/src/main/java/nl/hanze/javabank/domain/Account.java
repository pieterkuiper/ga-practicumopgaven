package nl.hanze.javabank.domain;

import java.io.Serializable;

/**
 *
 * @author Pieter
 */
public class Account implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private int rekeningnummer;
    private String naam;
    private String adres;
    private String postcode;
    private String woonplaats;
    private float saldo;
    private float limiet;

    public Account()
    {
    }

    public int getRekeningnummer()
    {
        return rekeningnummer;
    }

    public void setRekeningnummer(int rekeningnummer)
    {
        this.rekeningnummer = rekeningnummer;
    }

    public String getNaam()
    {
        return naam;
    }

    public void setNaam(String naam)
    {
        this.naam = naam;
    }

    public String getAdres()
    {
        return adres;
    }

    public void setAdres(String adres)
    {
        this.adres = adres;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getWoonplaats()
    {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats)
    {
        this.woonplaats = woonplaats;
    }

    public float getSaldo()
    {
        return saldo;
    }

    public void setSaldo(float saldo)
    {
        this.saldo = saldo;
    }

    public float getLimiet()
    {
        return limiet;
    }

    public void setLimiet(float limiet)
    {
        this.limiet = limiet;
    }
}
