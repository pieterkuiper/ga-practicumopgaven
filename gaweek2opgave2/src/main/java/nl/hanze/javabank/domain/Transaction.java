package nl.hanze.javabank.domain;

import java.util.Date;

/**
 *
 * @author Pieter
 */
public class Transaction
{
    private int transactionId;
    private int verzender;
    private int ontvanger;
    private float bedrag;
    private Date datum;

    public Transaction()
    {
    }

    public int getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(int transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getVerzender()
    {
        return verzender;
    }

    public void setVerzender(int verzender)
    {
        this.verzender = verzender;
    }

    public int getOntvanger()
    {
        return ontvanger;
    }

    public void setOntvanger(int ontvanger)
    {
        this.ontvanger = ontvanger;
    }

    public float getBedrag()
    {
        return bedrag;
    }

    public void setBedrag(float bedrag)
    {
        this.bedrag = bedrag;
    }

    public Date getDatum()
    {
        return datum;
    }

    public void setDatum(Date datum)
    {
        this.datum = datum;
    }
}
