package nl.hanze.javabank.dao;

import nl.hanze.javabank.domain.Account;
import nl.hanze.javabank.domain.Transaction;

/**
 *
 * @author Pieter
 */
public interface JavabankDAO
{
    public void openAccount(Account account);
    public Account getAccount(String reknummer);
    public void alterAccount(Account account);
    public boolean transfer(float amount, String reknummerFrom, String reknummerTo);
    public Transaction[] getTransactions(String reknummer, String date);
}
