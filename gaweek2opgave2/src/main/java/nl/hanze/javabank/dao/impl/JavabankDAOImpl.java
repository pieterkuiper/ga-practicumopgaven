package nl.hanze.javabank.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import nl.hanze.javabank.dao.JavabankDAO;
import nl.hanze.javabank.domain.Account;
import nl.hanze.javabank.domain.Transaction;

/**
 *
 * @author Pieter
 */
public class JavabankDAOImpl implements JavabankDAO
{
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost/javabank";
    private static final String DB_USER = "javabank_user";
    private static final String DB_PASSWORD = "javabank_user";
    
    private Connection connection;
    
    public JavabankDAOImpl()
    {
        try
        {
            openConnection();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    @Override
    public void openAccount(Account account)
    {
        String sql = "INSERT INTO rekening "
                + "(naam, adres, postcode, woonplaats, saldo, limiet) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try
        {
            int reknummer = -1;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS))
            {
                preparedStatement.setString(1, account.getNaam());
                preparedStatement.setString(2, account.getAdres());
                preparedStatement.setString(3, account.getPostcode());
                preparedStatement.setString(4, account.getWoonplaats());
                preparedStatement.setFloat(5, account.getSaldo());
                preparedStatement.setFloat(6, account.getLimiet());
                preparedStatement.executeUpdate();
                
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                
                if (resultSet.next())
                {
                    reknummer = resultSet.getInt(1);
                }
            }
            
            connection.commit();
            
            System.out.println("Rekening met rekeningnummer: " + reknummer + " voor " + account.getNaam() + " aangemaakt.");
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    @Override
    public Account getAccount(String reknummer)
    {
        Account account = null;
        String sql = "SELECT * FROM rekening "
                + "WHERE rekeningnummer = ?";
        
        try
        {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
            {
                preparedStatement.setInt(1, Integer.parseInt(reknummer));
                
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next())
                {
                    account = new Account();
                    account.setRekeningnummer(resultSet.getInt("rekeningnummer"));
                    account.setNaam(resultSet.getString("naam"));
                    account.setAdres(resultSet.getString("adres"));
                    account.setPostcode(resultSet.getString("postcode"));
                    account.setWoonplaats(resultSet.getString("woonplaats"));
                    account.setSaldo(resultSet.getFloat("saldo"));
                    account.setLimiet(resultSet.getFloat("limiet"));
                }
            }
            
            connection.commit();
        }
        catch (SQLException | NumberFormatException ex)
        {
            System.out.println(ex);
        }
        
        return account;
    }
    
    @Override
    public void alterAccount(Account account)
    {
        String sql = "UPDATE rekening SET naam = ?, "
                + "adres = ?, "
                + "postcode = ?, "
                + "woonplaats = ?, "
                + "saldo = ?, "
                + "limiet = ? "
                + "WHERE rekeningnummer = ?";
        try
        {
            int rows;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
            {
                preparedStatement.setString(1, account.getNaam());
                preparedStatement.setString(2, account.getAdres());
                preparedStatement.setString(3, account.getPostcode());
                preparedStatement.setString(4, account.getWoonplaats());
                preparedStatement.setFloat(5, account.getSaldo());
                preparedStatement.setFloat(6, account.getLimiet());
                preparedStatement.setInt(7, account.getRekeningnummer());
                rows = preparedStatement.executeUpdate();
            }
            
            connection.commit();
            
            if (rows > 0)
            {
                System.out.println("Update succesvol!");
            }
            else
            {
                System.out.println("Update niet succesvol!");
            }
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    @Override
    public boolean transfer(float amount, String reknummerFrom, String reknummerTo)
    {
        String sqlRekeningFrom = "UPDATE rekening SET saldo = ? "
                + "WHERE rekeningnummer = ?";
        String sqlRekeningTo = "UPDATE rekening SET saldo = ? "
                + "WHERE rekeningnummer = ?";
        String sqlTransactie = "INSERT INTO transactie "
                + "(verzender, ontvanger, bedrag, datum) "
                + "VALUES (?, ?, ?, ?)";
        
        Account accountFrom = getAccount(reknummerFrom);
        
        // Controleer of het rekeningnummer bestaat. Als accountFrom null is, bestaat het rekening nummer niet
        if (accountFrom == null)
        {
            System.out.println("accountFrom");
            return false;
        }
        
        float nieuwSaldoFrom = accountFrom.getSaldo() - amount;
        System.out.println("amount: " + amount);
        System.out.println("saldoFrom: " + accountFrom.getSaldo());
        System.out.println("nieuwSaldoFrom: " + nieuwSaldoFrom);
        System.out.println("LimietFrom: " + accountFrom.getLimiet());
        
        // Controleer of het saldo toereikend is
        if (nieuwSaldoFrom < accountFrom.getLimiet())
        {
            System.out.println("saldoFrom");
            return false;
        }
        
        Account accountTo = getAccount(reknummerTo);
        
        // Controleer of het rekeningnummer bestaat. Als accountTo null is, bestaat het rekening nummer niet
        if (accountTo == null)
        {
            System.out.println("accountTo");
            return false;
        }
        
        float nieuwSaldoTo = accountTo.getSaldo() + amount;
        
        try
        {
            int rowsFrom;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlRekeningFrom))
            {
                preparedStatement.setFloat(1, nieuwSaldoFrom);
                preparedStatement.setInt(2, Integer.parseInt(reknummerFrom));
                rowsFrom = preparedStatement.executeUpdate();
            }
            
            if (rowsFrom > 0)
            {
                System.out.println("Update from succesvol!");
            }
            else
            {
                System.out.println("Update from niet succesvol!");
            }
            
            int rowsTo;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlRekeningTo))
            {
                preparedStatement.setFloat(1, nieuwSaldoTo);
                preparedStatement.setInt(2, Integer.parseInt(reknummerTo));
                rowsTo = preparedStatement.executeUpdate();
            }
            
            if (rowsTo > 0)
            {
                System.out.println("Update to succesvol!");
            }
            else
            {
                System.out.println("Update to niet succesvol!");
            }
            
            int transactionId = -1;
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlTransactie, Statement.RETURN_GENERATED_KEYS))
            {
                preparedStatement.setInt(1, Integer.parseInt(reknummerFrom));
                preparedStatement.setInt(2, Integer.parseInt(reknummerTo));
                preparedStatement.setFloat(3, amount);
                preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                preparedStatement.executeUpdate();
                
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                
                if (resultSet.next())
                {
                    transactionId = resultSet.getInt(1);
                }
            }
            
            if (transactionId != -1)
            {
                System.out.println("Insert succesvol!");
                System.out.println("transactionId: " + transactionId);
            }
            else
            {
                System.out.println("Insert niet succesvol!");
            }

            connection.commit();
            
            System.out.println("Transactie succesvol.");
            System.out.println("Saldo " + accountFrom.getNaam() + " voor transactie: " + accountFrom.getSaldo());
            System.out.println("Saldo " + accountFrom.getNaam() + " na transactie: " + nieuwSaldoFrom);
            System.out.println("Saldo " + accountTo.getNaam() + " voor transactie: " + accountTo.getSaldo());
            System.out.println("Saldo " + accountTo.getNaam() + " na transactie: " + nieuwSaldoTo);
            
            return true;
        }
        catch (SQLException | NumberFormatException ex)
        {
            System.out.println(ex);
            
            try
            {
                connection.rollback();
            }
            catch (SQLException ex1)
            {
                System.out.println(ex1);
            }
            return false;
        }
    }
    
    @Override
    public Transaction[] getTransactions(String reknummer, String date)
    {
        Transaction[] transactions = null;
        int count = 0;
        String sqlCount = "SELECT COUNT(*) FROM transactie "
                + "WHERE (ontvanger = ? OR verzender = ?) AND datum > ?";
        String sql = "SELECT * FROM transactie "
                + "WHERE (ontvanger = ? OR verzender = ?) AND datum > ?";
        System.out.println("date: " + date);
        try
        {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date datum = df.parse(date);
            
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlCount))
            {
                preparedStatement.setInt(1, Integer.parseInt(reknummer));
                preparedStatement.setInt(2, Integer.parseInt(reknummer));
                preparedStatement.setTimestamp(3, new Timestamp(datum.getTime()));
                
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next())
                {
                    count = resultSet.getInt(1);
                }
            }
            
            transactions = new Transaction[count];
            
            System.out.println("Aantal transacties: " + count);
            
            if (count > 0)
            {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
                {
                    preparedStatement.setInt(1, Integer.parseInt(reknummer));
                    preparedStatement.setInt(2, Integer.parseInt(reknummer));
                    preparedStatement.setTimestamp(3, new Timestamp(datum.getTime()));

                    ResultSet resultSet = preparedStatement.executeQuery();
                    int i = 0;
                    while (resultSet.next())
                    {
                        Transaction transaction = new Transaction();
                        transaction.setTransactionId(resultSet.getInt("transaction_id"));
                        transaction.setVerzender(resultSet.getInt("verzender"));
                        transaction.setOntvanger(resultSet.getInt("ontvanger"));
                        transaction.setBedrag(resultSet.getFloat("bedrag"));
                        transaction.setDatum(resultSet.getTimestamp("datum"));
                        transactions[i] = transaction;
                        i++;
                    }
                }
            }
            
            connection.commit();
        }
        catch (ParseException | SQLException | NumberFormatException ex)
        {
            System.out.println(ex);
        }
        
        return transactions;
    }

    private void openConnection() throws ClassNotFoundException, SQLException
    {
        Class.forName(DB_DRIVER);
        connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        connection.setAutoCommit(false);
    }

    private void closeConnection()
    {
        try
        {
            if (connection != null)
            {
                connection.close();
            }
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    public static void main(String[] args) throws InterruptedException
    {
        Account account = new Account();
        account.setNaam("Pieter Kuiper");
        account.setAdres("Korenbloemstraat 24");
        account.setPostcode("9713PW");
        account.setWoonplaats("Groningen");
        account.setSaldo(965.95f);
        account.setLimiet(2000.00f);
        JavabankDAOImpl jdao = new JavabankDAOImpl();
        //jdao.openAccount(account);
        
        //account = jdao.getAccount("300000001");
        //System.out.println("rekeningnummer: " + account.getRekeningnummer() + " - naam: " + account.getNaam() + " - saldo: " + account.getSaldo());
        
        //account.setNaam("Henk Jansen");
        //account.setSaldo(account.getSaldo() + 34.05f);
        //jdao.alterAccount(account);
        
        //System.out.println(jdao.transfer(498.95f, "300000001", "300000000"));
        
        Transaction[] transactions = jdao.getTransactions("300000000", "04-12-2013");
        for (Transaction transaction : transactions)
        {
            System.out.println("transaction_id: " + transaction.getTransactionId() + 
                    " - verzender: " + transaction.getVerzender() + 
                    " - ontvanger: " + transaction.getOntvanger() + 
                    " - bedrag: " + transaction.getBedrag() + 
                    " - datum: " + transaction.getDatum());
        }
        jdao.closeConnection();
    }
}

