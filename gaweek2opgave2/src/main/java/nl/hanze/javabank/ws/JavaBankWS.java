package nl.hanze.javabank.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import nl.hanze.javabank.dao.JavabankDAO;
import nl.hanze.javabank.dao.impl.JavabankDAOImpl;
import nl.hanze.javabank.domain.Account;
import nl.hanze.javabank.domain.Transaction;

/**
 *
 * @author Pieter
 */
@WebService(serviceName = "JavaBankWS")
public class JavaBankWS
{
    JavabankDAO jdao = new JavabankDAOImpl();
    
    /**
     * Web service operation
     * @param account
     */
    @WebMethod(operationName = "openAccount")
    public void openAccount(@WebParam(name = "account") Account account)
    {
        if (account != null)
        {
            jdao.openAccount(account);
        }
    }

    /**
     * Web service operation
     * @param amount
     * @param reknummerFrom
     * @param reknummerTo
     * @return 
     */
    @WebMethod(operationName = "transfer")
    public boolean transfer(@WebParam(name = "amount") float amount,
            @WebParam(name = "reknummerFrom") String reknummerFrom,
            @WebParam(name = "reknummerTo") String reknummerTo)
    {
        if (amount != 0.0f && reknummerFrom != null && reknummerTo != null)
        {
            return jdao.transfer(amount, reknummerFrom, reknummerTo);
        }
        
        return false;
    }

    /**
     * Web service operation
     * @param reknummer
     * @return 
     */
    @WebMethod(operationName = "getAccount")
    public Account getAccount(@WebParam(name = "reknummer") String reknummer)
    {
        if (reknummer != null)
        {
            return jdao.getAccount(reknummer);
        }
        
        return null;
    }

    /**
     * Web service operation
     * @param account
     */
    @WebMethod(operationName = "alterAccount")
    public void alterAccount(@WebParam(name = "account") Account account)
    {
        if (account != null)
        {
            jdao.alterAccount(account);
        }
    }

    /**
     * Web service operation
     * @param reknummer
     * @param date
     * @return 
     */
    @WebMethod(operationName = "getTransactions")
    public Transaction[] getTransactions(@WebParam(name = "reknummer") String reknummer,
            @WebParam(name = "date") String date)
    {
        if (reknummer != null && date != null)
        {
            return jdao.getTransactions(reknummer, date);
        }
        
        return null;
    }
}
