DROP DATABASE IF EXISTS `javabank`;
CREATE DATABASE `javabank` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `javabank`;

CREATE TABLE IF NOT EXISTS `rekening` (
  `rekeningnummer` int(9) NOT NULL AUTO_INCREMENT,
  `naam` varchar(45) DEFAULT NULL,
  `adres` varchar(45) DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `woonplaats` varchar(45) DEFAULT NULL,
  `saldo` float(8,2) DEFAULT NULL,
  `limiet` float(8,2) DEFAULT NULL,
  PRIMARY KEY (`rekeningnummer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=300000000 ;

CREATE TABLE IF NOT EXISTS `transactie` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `verzender` int(9) DEFAULT NULL,
  `ontvanger` int(9) DEFAULT NULL,
  `bedrag` float(8,2) DEFAULT NULL,
  `datum` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;