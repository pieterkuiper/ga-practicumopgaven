<?php
	$options = array('trace' => 1);
	$client = new SoapClient('http://localhost:8080/gaweek2opgave2/JavaBankWS?wsdl', $options);
	
	print "FUNCTIONS:\n";
	var_dump($client->__getFunctions());
	
	print "\nDATATYPES:\n";
	print_r( $client->__getTypes());

	print "\nSOAP CALL:\n";
	$response = $client->alterAccount(array(
		'account' => array(
			'rekeningnummer' => 300000003, 
			'naam' => 'Wesley Kappen', 
			'adres' => 'Adres 123', 
			'postcode' => '9713PW', 
			'woonplaats' => 'Groningen', 
			'saldo' => 1000.00, 
			'limiet' => -200.00)));
	
	print "\nSOAP REQUEST:\n";
	var_dump ($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump ($client->__getLastResponse());

	print "\n\n";
