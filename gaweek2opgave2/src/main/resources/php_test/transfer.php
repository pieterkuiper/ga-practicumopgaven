<?php
	$options = array('trace' => 1);
	$client = new SoapClient('http://localhost:8080/gaweek2opgave2/JavaBankWS?wsdl', $options);
	
	print "FUNCTIONS:\n";
	var_dump($client->__getFunctions());
	
	print "\nDATATYPES:\n";
	print_r( $client->__getTypes());

	print "\nSOAP CALL:\n";
	$response = $client->transfer(array(
		'amount' => 101.15, 
		'reknummerFrom' => '300000002', 
		'reknummerTo' => '300000003'));
	
	print "\nSOAP REQUEST:\n";
	var_dump($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump($client->__getLastResponse());
	
	print "\nreturn:\n";
	$data = $response->return;
	var_dump($data);

	print "\n\n";
