<?php
	$client = new SoapClient('http://localhost:8080/gaweek2opgave1/StudentWSService?wsdl', array('trace'=>1));

	print "FUNCTIONS OF THE SERVICE:\n"; 
	var_dump ($client->__getFunctions());
	
	print "DATATYPES OF THE SERVICE:\n"; 
	print_r($client->__getTypes());

	print "\nSOAP CALL:\n";
	$response = $client->getStudent(array('arg0' => 'Pieter Kuiper'));
	var_dump ($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump ($client->__getLastResponse());
	
	print "\nXML:\n";
	print $response->return;

	print "\n\n";
