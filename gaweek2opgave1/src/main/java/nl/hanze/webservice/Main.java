package nl.hanze.webservice;

import javax.xml.bind.JAXBException;

/**
 *
 * @author Pieter
 */
public class Main
{
    public static void main(String[] args) throws JAXBException
    {
        Student student = new Student();
        student.setNaam("Pieter Kuiper");
        student.setLeeftijd(35);
        student.setGeslacht(true);
        toXML(student);
        toXMLJAXB(student);
    }
    
    private static void toXML(Student student)
    {
        System.out.println(student.toXML());
    }
    
    private static void toXMLJAXB(Student student) throws JAXBException
    {
        System.out.println(student.toXMLJAXB());
    }
}
