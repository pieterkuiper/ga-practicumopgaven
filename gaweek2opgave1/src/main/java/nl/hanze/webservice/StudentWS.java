package nl.hanze.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Pieter
 */
@WebService
public class StudentWS
{
    @WebMethod
    public String getStudent(String naam)
    {
        Student student = new Student();
        student.setNaam(naam);
        student.setLeeftijd(35);
        student.setGeslacht(true);
        
        return student.toXML();
    }
}
