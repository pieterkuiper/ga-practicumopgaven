package nl.hanze.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "naam", "leeftijd", "geslacht" })
@XmlRootElement(name = "student")
@XStreamAlias("student")
public class Student
{
    @XmlElement(required = true)
    private String naam;
    @XmlElement(required = true)
    private int leeftijd;
    @XmlElement(required = true)
    private boolean geslacht;
	
    public String getNaam()
    {
        return naam;
    }
    
    public void setNaam(String naam)
    {
        this.naam = naam;
    }
    
    public int getLeeftijd()
    {
        return leeftijd;
    }
    
    public void setLeeftijd(int leetijd)
    {
        this.leeftijd = leetijd;
    }
    
    public boolean isGeslacht()
    {
        return geslacht;
    }
    
    public void setGeslacht(boolean geslacht)
    {
        this.geslacht = geslacht;
    }
    
    protected String toXML()
    {
        //week 2, opgave 1a
        XStream xstream = new XStream(new StaxDriver());
        xstream.processAnnotations(Student.class);
        
        return xstream.toXML(this);
    }
    
    protected String toXMLJAXB() throws JAXBException
    {
        //week 2, opgave 1a
        StringWriter sw = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(Student.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(this, sw);
        
        return sw.toString();
    }
}
