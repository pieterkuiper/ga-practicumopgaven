#!/bin/bash
mydir="$(dirname "$BASH_SOURCE")"
echo $mydir/bin
cd $mydir/bin
echo Start AddressBookServer
java nl.hanze.web.homegrownrpc.addressbook.AddressBookServer
