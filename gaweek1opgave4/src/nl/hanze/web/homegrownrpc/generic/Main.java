package nl.hanze.web.homegrownrpc.generic;

public class Main
{
	// Opgave 4g
	public static void main(String[] args) throws Exception
	{
		System.out.println("Genereer AddresBook.java");
    	System.out.println("Locatie: " + System.getProperty("user.dir"));
        StubGenerator sg = new StubGenerator("nl.hanze.web.homegrownrpc.addressbook.AddressBook", System.getProperty("user.dir"));
        sg.generateStub();
	}
}
