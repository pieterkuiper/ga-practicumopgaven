package nl.hanze.web.homegrownrpc.generic;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;

public class StubGenerator
{
    private StringBuffer stbTemp;
    private String fullInterfaceName;
    private String nameStub;
    private String packageStub;
    private String locationStub;
    private HashMap<String, String[]> wrapperClass;

    public StubGenerator(String fullInterfaceName, String baseLocationStub)
    {
        this.fullInterfaceName = fullInterfaceName;
        this.nameStub = getInterfaceName() + "Stub";
        this.packageStub = getPackageName(false);
        this.locationStub = baseLocationStub + File.separatorChar + getPackageName(true);
        // Opgave 4a
        wrapperClass = new HashMap<String, String[]>();
        wrapperClass.put("byte", new String[]{"java.lang.Byte", "byteValue()"});
        wrapperClass.put("short", new String[]{"java.lang.Short", "shortValue()"});
        wrapperClass.put("int", new String[]{"java.lang.Integer", "intValue()"});
        wrapperClass.put("long", new String[]{"java.lang.Long", "longValue()"});
        wrapperClass.put("float", new String[]{"java.lang.Float", "floatValue()"});
        wrapperClass.put("double", new String[]{"java.lang.Double", "doubleValue()"});
        wrapperClass.put("boolean", new String[]{"java.lang.Boolean", "booleanValue()"});
        wrapperClass.put("char", new String[]{"java.lang.Character", "charValue()"});
    }

    public void generateStub() throws Exception
    {
        stbTemp = new StringBuffer();
        generatePackageName();
        generateEmptyLine();
        generateClassLine();
        Method[] methods = searchMethods();
        for (Method method : methods)
        {
            generateEmptyLine();
            generateMethod(method);
        }
        generateEmptyLine();
        generateLastLine();
        saveGeneratedClass();
    }

    private void generatePackageName()
    {
        if (packageStub != null && packageStub.length() != 0)
        {
        	// Opgave 4c
        	stbTemp.append("package " + packageStub + ";\n");
        }
    }

    private void generateClassLine()
    {
        stbTemp.append("public class ");
        stbTemp.append(nameStub);
        // Opgave 4b
        stbTemp.append(" extends nl.hanze.web.homegrownrpc.generic.Stub implements " + fullInterfaceName + "\n{");
    }

    private Method[] searchMethods() throws Exception
    {
    	// Opgave 4d
        @SuppressWarnings("rawtypes")
		Class className = Class.forName(fullInterfaceName);
        return className.getMethods();
    }

    private void generateMethod(Method method)
    {
    	String returnType = method.getReturnType().getCanonicalName();
        boolean isPrimitive = wrapperClass.containsKey(returnType);
        boolean isVoid = returnType.equals("void");
		String methodName = method.getName();
        @SuppressWarnings("rawtypes")
		Class[] parameters = method.getParameterTypes();
		boolean hasParameters = parameters.length != 0;
        @SuppressWarnings("rawtypes")
        Class[] exceptions = method.getExceptionTypes();
        boolean hasExceptions = exceptions.length != 0;
        
        // Opgave 4e
        stbTemp.append("\tpublic ");
        stbTemp.append(returnType);
        stbTemp.append(" ");
        stbTemp.append(methodName);
        stbTemp.append("(");
        // Parameters toevoegen
        for (int i = 0; i < parameters.length; i++)
        {
        	if (i != 0)
        	{
        		stbTemp.append(", ");
        	}
        	stbTemp.append(parameters[i].getCanonicalName() + " arg" + i);
        }
        stbTemp.append(")");
        
        // Exceptions toevoegen
        if (hasExceptions)
        {
        	stbTemp.append(" throws ");
        	for (int i = 0; i < exceptions.length; i++)
            {
            	if (i != 0)
            	{
            		stbTemp.append(", ");
            	}
            	stbTemp.append(exceptions[i].getCanonicalName());
            }
        }
        
        // Nieuwe regel
        stbTemp.append("\n\t{\n\t\t");
        
        if(!isVoid)
        {
        	stbTemp.append("java.lang.Object o = ");
        }
        stbTemp.append("invokeSkel(\"");
        stbTemp.append(methodName);
        stbTemp.append("\", ");
        
        if (hasParameters)
        {
        	stbTemp.append("new java.lang.Class[] {");
        	for (int i = 0; i < parameters.length; i++)
        	{
            	if (i != 0)
            	{
            		stbTemp.append(", ");
            	}
        		stbTemp.append(parameters[i].getCanonicalName());
        		stbTemp.append(".class");
        	}
        	stbTemp.append("}, new java.lang.Object[] {");
        	for (int i = 0; i < parameters.length; i++)
        	{
            	if (i != 0)
            	{
            		stbTemp.append(", ");
            	}
        		stbTemp.append("arg"+i);
        	}
        	stbTemp.append("}");
        }
        else
        {
        	stbTemp.append("null, null");
        }
        stbTemp.append(");\n");
        
        if(!isVoid)
        {
        	stbTemp.append("\t\treturn (");
        	if (isPrimitive)
        	{
        		stbTemp.append("(");
        		stbTemp.append(wrapperClass.get(returnType)[0]);
        		stbTemp.append(") o).");
        		stbTemp.append(wrapperClass.get(returnType)[1]);
        	}
        	else
        	{
        		stbTemp.append(returnType);
        		stbTemp.append(") o");
        	}
        	stbTemp.append(";\n");
        }
        stbTemp.append("\t}\n");
    }

    private void generateEmptyLine()
    {
        stbTemp.append("\n");
    }

    private void generateLastLine()
    {
        stbTemp.append("}\n");
    }

    private void saveGeneratedClass() throws Exception
    {
        File file = new File(locationStub);
        file.mkdirs();
        // Opgave 4f
        FileOutputStream fos = new FileOutputStream(file.getAbsolutePath() + File.separatorChar + nameStub + ".java");
        fos.write(stbTemp.toString().getBytes());
        fos.flush();
        fos.close();
    }

    private String getInterfaceName()
    {
        int i = fullInterfaceName.lastIndexOf('.');
        if (i == -1)
    	{
        	return fullInterfaceName;
    	}
        i++;
        return fullInterfaceName.substring(i);
    }

    private String getPackageName(boolean useSlashes)
    {
        int i = fullInterfaceName.lastIndexOf('.');
        if (i == -1)
    	{
        	return "";
    	}
        if (useSlashes)
        {
            return fullInterfaceName.substring(0, i).replace('.', File.separatorChar);
        }
        else
        {
            return fullInterfaceName.substring(0, i);
        }
    }

    public static void main(String[] args) throws Exception
    {
    	System.out.println("Genereer AddresBook.java");
    	System.out.println("Locatie: " + System.getProperty("user.dir"));
        StubGenerator sg = new StubGenerator("nl.hanze.web.homegrownrpc.addressbook.AddressBook", System.getProperty("user.dir"));
        sg.generateStub();
    }
}
