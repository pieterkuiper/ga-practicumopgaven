package nl.hanze.web.homegrownrpc.addressbook;

public class AddressBookStub extends nl.hanze.web.homegrownrpc.generic.Stub implements nl.hanze.web.homegrownrpc.addressbook.AddressBook
{
	public java.util.List getAllStudentsAsList() throws java.lang.Exception
	{
		java.lang.Object o = invokeSkel("getAllStudentsAsList", null, null);
		return (java.util.List) o;
	}

	public nl.hanze.web.homegrownrpc.addressbook.Student[] getAllStudentsAsArray() throws java.lang.Exception
	{
		java.lang.Object o = invokeSkel("getAllStudentsAsArray", null, null);
		return (nl.hanze.web.homegrownrpc.addressbook.Student[]) o;
	}

	public void addStudent(nl.hanze.web.homegrownrpc.addressbook.Student arg0) throws java.lang.Exception
	{
		invokeSkel("addStudent", new java.lang.Class[] {nl.hanze.web.homegrownrpc.addressbook.Student.class}, new java.lang.Object[] {arg0});
	}

	public boolean removeStudent(int arg0) throws java.lang.Exception
	{
		java.lang.Object o = invokeSkel("removeStudent", new java.lang.Class[] {int.class}, new java.lang.Object[] {arg0});
		return ((java.lang.Boolean) o).booleanValue();
	}

	public int countStudents() throws java.lang.Exception
	{
		java.lang.Object o = invokeSkel("countStudents", null, null);
		return ((java.lang.Integer) o).intValue();
	}

}
