<?php
	$client = new SoapClient('http://localhost:8080/gaweek2opgave5/Thesaurus?wsdl', array('trace' => 1));
	
	print "FUNCTIONS OF THE SERVICE:\n";
	var_dump ($client->__getFunctions());
	
	print "DATATYPES OF THE SERVICE:\n";
	print_r( $client->__getTypes());

	print "\nSOAP CALL:\n";
	$response = $client->GetInfo(array(
		'kenteken' => array(
			'kenteken' => 'DIT KLOPT NIET')));
	
	print "\nSOAP REQUEST:\n";
	var_dump ($client->__getLastRequest());

	print "\nSOAP RESPONSE:\n";
	var_dump ($client->__getLastResponse());
	
	print "\nreturn:\n";
	$data = $response->return;
	var_dump($data);

	print "\n\n";
