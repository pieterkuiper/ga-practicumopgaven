package nl.hanze.web.rdw.db;

import java.util.HashMap;
import nl.hanze.web.rdw.service.Info;
import nl.hanze.web.rdw.service.Kenteken;
import nl.hanze.web.rdw.service.Model;

/**
 *
 * @author Pieter
 */
public class Database
{
    private final HashMap<String, Info> database;
    
    public Database()
    {
        database = new HashMap<>();    
        addInfoToDatabase();
    }
    
    private void addInfoToDatabase()
    {
        Info info1 = createInfo(485937992, "JMZNA18B200120990", "TT-73-KG", "Wit", "Mazda", 1800f, "MX-5", "B");
        database.put("TT-73-KG", info1);
        Info info2 = createInfo(868795550, "WVWZZZ1KZ4B002803", "97-LH-GS", "Rood", "VW", 2000f, "Golf V", "D");
        database.put("97-LH-GS", info2);
        Info info3 = createInfo(136803908, "JTDKB20UX60999999", "01-VBBB-1", "Grijs", "Toyota", 1300f, "Aygo", "L");
        database.put("01-VBBB-1", info3);
    }
    
    private Info createInfo(long bsn, String chassisNummer, 
            String kenteken, String kleur, String merkNaam, 
            float motorInhoud, String typeAanduiding, String typeMotor)
    {
        Info info = new Info();
        info.setBsn(bsn);
        info.setChassisNummer(chassisNummer);
        info.setKenteken(kenteken);
        info.setKleur(kleur);
        
        Model model = new Model();
        model.setMerkNaam(merkNaam);
        model.setMotorInhoud(motorInhoud);
        model.setTypeAanduiding(typeAanduiding);
        model.setTypeMotor(typeMotor);       
        
        info.setModel(model); 
        
        return info;
    }

    public Info getInfo(Kenteken kenteken)
    {
        Info info;
        String kentekenString = "";
            
        if (kenteken != null)
        {
            kentekenString = kenteken.getKenteken();
        }
        
        if (!kentekenString.isEmpty() && database.containsKey(kentekenString))
        {
            info = database.get(kentekenString);
            info.setStatusInfoAanvraag("OK");
        }
        else
        {
            info = createInfo(0, "", kentekenString, "", "", 0, "", "");
            info.setStatusInfoAanvraag("NOT FOUND");
        }
        
        return info;
    } 
}
