package nl.hanze.web.rdw.ws;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import nl.hanze.web.rdw.db.Database;
import nl.hanze.web.rdw.service.Info;
import nl.hanze.web.rdw.service.Kenteken;

/**
 *
 * @author Pieter
 */
@WebService(serviceName = "Thesaurus")
public class Thesaurus
{
    @Resource(mappedName = "jms/boetebepalingFactory")
    private ConnectionFactory connectionFactory;
    @Resource(mappedName = "jms/boetebepaling")
    private Queue queue;
    
    Database database = new Database();
    
    /**
     * Web service operation
     * @param kenteken
     * @return 
     */
    @WebMethod(operationName = "GetInfo")
    public Info getInfo(@WebParam(name = "kenteken") Kenteken kenteken)
    {
        if (kenteken != null)
        {
            System.out.println("Kenteken: " + kenteken.getKenteken());
        }
        else
        {
            System.out.println("Kenteken: null");
        }
        
        return database.getInfo(kenteken);
    }
    
    @WebMethod(operationName = "berekenBoete")
    public String berekenBoete(@WebParam(name = "bsn") int bsn, 
            @WebParam(name = "limiet") int limiet, @WebParam(name = "snelheid") int snelheid)
    {
        if (limiet > snelheid)
        {
            return "Limiet moet kleiner zijn dan de snelheid";
        }
        
        try
        {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(queue);
            
            TextMessage textMessage = session.createTextMessage();
            textMessage.setIntProperty("bsn", bsn);
            textMessage.setIntProperty("limiet", limiet);
            textMessage.setIntProperty("snelheid", snelheid);
            
            messageProducer.send(textMessage);
            messageProducer.close();
            connection.close();
        }
        catch (JMSException ex)
        {
            System.out.println("error: " + ex.getMessage());
            return "error";
        }
        
        return "Snelheidsovertreding in jms/boetebepaling queue gezet";
    }
}
