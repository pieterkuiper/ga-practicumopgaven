package nl.hanze.web.gba;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.hanze.web.gba.dao.NatuurlijkPersoonDAO;
import nl.hanze.web.gba.dao.impl.NatuurlijkPersoonDAOImpl;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;
import nl.hanze.web.gba.domain.Status;
import nl.hanze.web.gba.security.AESCodec;
import nl.hanze.web.gba.util.StringUtil;

public class GBA extends HttpServlet
{
    NatuurlijkPersoonDAO npdao = new NatuurlijkPersoonDAOImpl();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println("doGet");
        String action = readAction(request);
        if ("get".equals(action))
        {
            System.out.println("get");
            long bsn = StringUtil.stringToLong(readBsn(request));
            NatuurlijkPersoon natuurlijkPersoon = npdao.getNatuurlijkPersoonbyBSN(bsn);
            
            // return json format
            request.setAttribute("np", natuurlijkPersoon);
            RequestDispatcher view = request.getRequestDispatcher("get.jsp");
            view.forward(request, response);
        }
        else
        {
            response.sendError(400, "Bad Request");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println("doPost");
        String action = readAction(request);
        if ("add".equals(action))
        {
            System.out.println("add");
            
            Status status = getStatus(request);
            
            // return json format
            response.setStatus(status.getHttpStatusCode());
            request.setAttribute("status", status);
            RequestDispatcher view = request.getRequestDispatcher("add.jsp");
            view.forward(request, response);
        }
        else
        {
            response.sendError(400, "Bad Request");
        }
    }

    private String readAction(HttpServletRequest request)
    {
        return request.getParameter("action");
    }
    
    private String readBsn(HttpServletRequest request)
    {
        return request.getParameter("bsn");
    }
    
    private String readNatuurlijkPersoon(HttpServletRequest request)
    {
        return request.getParameter("natuurlijkpersoon");
    }
    
    private Status getStatus(HttpServletRequest request)
    {
        Status status = new Status();
        status.setStatusCode("NOT OK");
        
        // JSON in parameter
        //String jsonEncrypted = readNatuurlijkPersoon(request);
        
        // JSON in body
        StringBuilder sb = new StringBuilder();
        try
        {
            try (BufferedReader reader = request.getReader())
            {
                String line;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }
            }
        }
        catch (IOException ex)
        {
            status.setStatusMessage("Unknown error");
            status.setHttpStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            
            return status;
        }
        String jsonEncrypted = sb.toString();
        
        System.out.println("jsonEncrypted: " + jsonEncrypted);
        
        if (jsonEncrypted == null || jsonEncrypted.isEmpty())
        {
            status.setStatusMessage("Empty encrypted JSON received");
            status.setHttpStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            
            return status;
        }
        
        System.out.println(jsonEncrypted.length());
        
        if (jsonEncrypted.length() <= 56)
        {
            status.setStatusMessage("Wrong message received");
            status.setHttpStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            
            return status;
        }
        
        AESCodec aesCodec = new AESCodec("quA$$_pl0L8L%gW5");
        //AESCodec aesCodec = new AESCodec("dit is niet goed");
        String json;
        try
        {
            json = aesCodec.decode(jsonEncrypted);
        }
        catch (Exception ex)
        {
            status.setStatusMessage(ex.getMessage());
            status.setHttpStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            
            return status;
        }

        if (json == null || json.isEmpty())
        {
            status.setStatusMessage("Empty JSON received");
            status.setHttpStatusCode(HttpServletResponse.SC_BAD_REQUEST);
            
            return status;
        }

        NatuurlijkPersoon natuurlijkPersoon = StringUtil.jsonToNatuurlijkPersoon(json);

        if (natuurlijkPersoon == null)
        {
            status.setStatusMessage("JSON parse error");
            status.setHttpStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            
            return status;
        }

        boolean created = npdao.createNatuurlijkPersoon(natuurlijkPersoon);
        if (!created)
        {
            status.setStatusMessage("BSN already exist");
            status.setHttpStatusCode(HttpServletResponse.SC_CONFLICT);
        }
        else
        {
            status.setStatusCode("OK");
            status.setStatusMessage("Add successful");
            status.setHttpStatusCode(HttpServletResponse.SC_CREATED);
        }
        
        return status;
    }
}
