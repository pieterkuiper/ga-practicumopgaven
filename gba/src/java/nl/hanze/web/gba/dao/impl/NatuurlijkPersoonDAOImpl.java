package nl.hanze.web.gba.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import nl.hanze.web.gba.dao.NatuurlijkPersoonDAO;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;

/**
 *
 * @author Pieter
 */
public class NatuurlijkPersoonDAOImpl implements NatuurlijkPersoonDAO
{
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost/gba";
    private static final String DB_USER = "gba_hanze";
    private static final String DB_PASSWORD = "gba_hanze";
    
    private Connection connection;

    public NatuurlijkPersoonDAOImpl()
    {
        try
        {
            openConnection();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    @Override
    public NatuurlijkPersoon getNatuurlijkPersoonbyBSN(long bsn)
    {
        System.out.println("bsn: " + bsn);
        
        if (bsn == -1)
        {
            return createEmptyNatuurlijkPersoon();
        }
        
        NatuurlijkPersoon natuurlijkPersoon = null;
        
        String sql = "SELECT * FROM natuurlijk_persoon "
                + "WHERE bsn = ?";
        
        try
        {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
            {
                preparedStatement.setLong(1, bsn);
                
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next())
                {
                    natuurlijkPersoon = new NatuurlijkPersoon();
                    natuurlijkPersoon.setBsn(resultSet.getLong("bsn"));
                    natuurlijkPersoon.setGeslacht(resultSet.getString("geslacht").charAt(0));
                    natuurlijkPersoon.setInitialen(resultSet.getString("initialen"));
                    natuurlijkPersoon.setAchternaam(resultSet.getString("achternaam"));
                    natuurlijkPersoon.setStraatnaam(resultSet.getString("straatnaam"));
                    natuurlijkPersoon.setNummer(resultSet.getInt("nummer"));
                    natuurlijkPersoon.setPostcode(resultSet.getString("postcode"));
                    natuurlijkPersoon.setWoonplaats(resultSet.getString("woonplaats"));
                }
                else
                {
                    natuurlijkPersoon = createEmptyNatuurlijkPersoon();
                }
            }
            
            connection.commit();
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
        }
        
        return natuurlijkPersoon;
    }
    
    @Override
    public boolean createNatuurlijkPersoon(NatuurlijkPersoon natuurlijkPersoon)
    {
        boolean created = false;
        
        if (getNatuurlijkPersoonbyBSN(natuurlijkPersoon.getBsn()).getBsn() == -1)
        {
            String sql = "INSERT INTO natuurlijk_persoon "
                    + "(bsn, geslacht, initialen, achternaam, straatnaam, nummer, postcode, woonplaats) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            try
            {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
                {
                    preparedStatement.setLong(1, natuurlijkPersoon.getBsn());
                    preparedStatement.setString(2, Character.toString(natuurlijkPersoon.getGeslacht()));
                    preparedStatement.setString(3, natuurlijkPersoon.getInitialen());
                    preparedStatement.setString(4, natuurlijkPersoon.getAchternaam());
                    preparedStatement.setString(5, natuurlijkPersoon.getStraatnaam());
                    preparedStatement.setInt(6, natuurlijkPersoon.getNummer());
                    preparedStatement.setString(7, natuurlijkPersoon.getPostcode());
                    preparedStatement.setString(8, natuurlijkPersoon.getWoonplaats());
                    preparedStatement.executeUpdate();
                }

                connection.commit();

                created = true;

                System.out.println("NatuurlijkPersoon met bsn: " + natuurlijkPersoon.getBsn() + " aangemaakt.");
            }
            catch (SQLException ex)
            {
                System.out.println(ex);
            }
        }
        
        return created;
    }
    
    private NatuurlijkPersoon createEmptyNatuurlijkPersoon()
    {
        NatuurlijkPersoon natuurlijkPersoon = new NatuurlijkPersoon();
        natuurlijkPersoon.setBsn(-1);
        natuurlijkPersoon.setGeslacht('E');
        natuurlijkPersoon.setInitialen("");
        natuurlijkPersoon.setAchternaam("");
        natuurlijkPersoon.setStraatnaam("");
        natuurlijkPersoon.setNummer(0);
        natuurlijkPersoon.setPostcode("");
        natuurlijkPersoon.setWoonplaats("");
        
        return natuurlijkPersoon;
    }
    
    private void openConnection() throws ClassNotFoundException, SQLException
    {
        Class.forName(DB_DRIVER);
        connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        connection.setAutoCommit(false);
    }

    private void closeConnection()
    {
        try
        {
            if (connection != null)
            {
                connection.close();
            }
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
        }
    }
    
    public static void main(String[] args)
    {
        NatuurlijkPersoonDAOImpl npdao = new NatuurlijkPersoonDAOImpl();
        NatuurlijkPersoon natuurlijkPersoon = npdao.getNatuurlijkPersoonbyBSN(123456789);
        npdao.closeConnection();
        
        System.out.println("bsn: " + natuurlijkPersoon.getBsn() + " - geslacht: " + natuurlijkPersoon.getGeslacht() + 
                " - initialen: " + natuurlijkPersoon.getInitialen() + " - achternaam: " + natuurlijkPersoon.getAchternaam() + 
                " - straatnaam: " + natuurlijkPersoon.getStraatnaam() + " - nummer: " + natuurlijkPersoon.getNummer() + 
                " - postcode: " + natuurlijkPersoon.getPostcode() + " - woonplaats: " + natuurlijkPersoon.getWoonplaats());
    }
}

