package nl.hanze.web.gba.dao;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

/**
 *
 * @author Pieter
 */
public interface NatuurlijkPersoonDAO
{
    public NatuurlijkPersoon getNatuurlijkPersoonbyBSN(long bsn);
    public boolean createNatuurlijkPersoon(NatuurlijkPersoon natuurlijkPersoon);
}
