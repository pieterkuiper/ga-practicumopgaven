package nl.hanze.web.gba.util;

import java.io.IOException;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Pieter
 */
public class StringUtil
{
    public static long stringToLong(String string)
    {
        if (string != null)
        {
            try
            {
                return Long.parseLong(string);
            }
            catch(NumberFormatException ex)
            {
                System.out.println("NumberFormatException: " + ex.getMessage());
            }
    	}
    	return -1L;
    }
    
    public static NatuurlijkPersoon jsonToNatuurlijkPersoon(String json)
    {
        NatuurlijkPersoon natuurlijkPersoon = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            natuurlijkPersoon = objectMapper.readValue(json, NatuurlijkPersoon.class);
        }
        catch (IOException ex)
        {
            System.out.println("error: " + ex.getMessage());
            System.out.println("json: " + json);
        }
        
        return natuurlijkPersoon;
    }
}
