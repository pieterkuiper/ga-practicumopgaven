package nl.hanze.web.gba.test;

import nl.hanze.web.gba.security.AESCodec;

/**
 *
 * @author Pieter
 */
public class Test
{
    public static void main(String[] args) throws Exception
    {
        AESCodec aesCodec = new AESCodec("quA$$_pl0L8L%gW5");
        
        // encode message in CBC mode
        String s = aesCodec.encode("{\"bsn\":\"123456789\",\"geslacht\":\"M\",\"initialen\": \"W\",\"achternaam\":\"Kappen\",\"straatnaam\":\"Straatnaam\",\"nummer\":\"123\",\"postcode\":\"9713PW\",\"woonplaats\":\"Groningen\"}");
        
        // write hex coded string
        System.out.println(s);
        
        // decode message
        String t = aesCodec.decode(s);
        
        // write decoded string
        System.out.println(t);
    }
}
