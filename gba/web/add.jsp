<%@page contentType="application/json" pageEncoding="UTF-8"%>
{
    "status": "${status.statusCode}",
    "message": "${status.statusMessage}"
}
